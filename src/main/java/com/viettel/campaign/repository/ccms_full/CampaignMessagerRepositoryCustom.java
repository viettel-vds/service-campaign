package com.viettel.campaign.repository.ccms_full;
import com.viettel.campaign.model.ccms_full.CampaignMessager;
import com.viettel.campaign.web.dto.CampaignMessagerDTO;
import com.viettel.campaign.web.dto.ResultDTO;

public interface CampaignMessagerRepositoryCustom {
    ResultDTO getCampaignMessagerList(CampaignMessagerDTO dto);
    ResultDTO getCampaignMessagerListExt(Integer currentPage, Integer perPage,CampaignMessagerDTO requestDto);
    ResultDTO getCampaignMessagerLogList(Integer currentPage, Integer perPage,Long messagerId, Integer timezoneOffset);
    String getLastID(String Prefix);
    boolean InsertMessager(CampaignMessager item);
    CampaignMessagerDTO GetMessagerInId(String messagerCode);
}
