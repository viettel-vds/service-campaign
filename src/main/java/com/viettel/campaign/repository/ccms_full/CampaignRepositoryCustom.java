package com.viettel.campaign.repository.ccms_full;

import com.viettel.campaign.model.ccms_full.*;
import com.viettel.campaign.web.dto.*;
import com.viettel.campaign.web.dto.request_dto.CampaignBlackListCatDTO;
import com.viettel.campaign.web.dto.request_dto.CampaignRequestDTO;
import com.viettel.campaign.web.dto.request_dto.CampaignTypeBlackListRequestDTO;
import com.viettel.econtact.filter.UserSession;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CampaignRepositoryCustom {

    ResultDTO search(CampaignRequestDTO requestDto);

    ResultDTO searchKeyWord(CampaignRequestDTO requestDto);

    Page<CampaignDTO> findByCampaignCode(CampaignRequestDTO requestDTO);

    String getMaxCampaignIndex();

    ResultDTO checkAllowStatusToPrepare(Long campaignId);

    //<editor-fold: hungtt>
    ResultDTO findCustomerListReallocation(CampaignRequestDTO dto);

    ResultDTO getListFieldsNotShow(CampaignRequestDTO dto);

    ResultDTO getListFieldsToShow(CampaignRequestDTO dto);

    ResultDTO getCampaignCustomerList(CampaignRequestDTO dto);

    ResultDTO getCustomerList(CampaignRequestDTO dto);

    ResultDTO getCustomerChoosenList(CampaignRequestDTO dto);

    List<Customer> getAllCutomerNotInCampagin(CampaignRequestDTO dto);

    CampaignInformationDTO getCampaignCustomerInformation(CampaignRequestDTO dto);

    List<CampaignInformationDTO> getCustomerListInformation(CampaignRequestDTO dto);

    List<CampaignInformationDTO> getCountIndividualOnList(CampaignRequestDTO dto);

    ResultDTO getCompanySiteId(Long companyId, Long siteId);

    List<CampaignCustomer> checkCusBeforeUpdate(Long customerId);

    Campaign getCampaignById(Long campaignId);
    //</editor-fold>

    List<CampaignTypeBlackListRequestDTO>findAllBlackListConfig();

    ResultDTO findCampaignWarning(CampaignRequestDTO requestDto);

    List<CalendarTimeDTO> getCalendarTime(String site_id,Integer day);
    List<CalendarHolidayDTO>getCalendarHoliday(String site_id);
    Boolean ckCalendarFullTime(String site_id);
    Boolean ckListCalendar(String site_id);

    List<CampaignBlackListCatDTO> findBlackCatByCampaignType(Long campaignType);

    String findBlackListConfigByCampaignTypeId(Long campaignTypeId);


}
