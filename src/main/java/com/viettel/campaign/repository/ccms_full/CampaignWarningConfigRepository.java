package com.viettel.campaign.repository.ccms_full;

import com.viettel.campaign.model.ccms_full.Campaign;
import com.viettel.campaign.model.ccms_full.CampaignWarningConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CampaignWarningConfigRepository extends JpaRepository<CampaignWarningConfig, Long> {

    Page<CampaignWarningConfig> findAll(Pageable pageable);

    List<CampaignWarningConfig> findAllByCampaignId(Long campaignId);

    @Query(value = "DELETE FROM CAMPAIGN_WARNING_CONFIG WHERE CAMPAIGN_ID = :campaignId ", nativeQuery = true)
    List<CampaignWarningConfig> removeByCampaignId(Long campaignId);
}
