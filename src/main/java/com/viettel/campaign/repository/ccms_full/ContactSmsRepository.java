package com.viettel.campaign.repository.ccms_full;

import com.viettel.campaign.config.DataSourceQualify;
import com.viettel.campaign.model.ccms_full.ContactSms;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.viettel.campaign.web.dto.request_dto.ContactSmsDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(DataSourceQualify.CCMS_FULL)
public interface ContactSmsRepository extends JpaRepository<ContactSms, Long> {
    @Query("SELECT C FROM ContactSms C WHERE LOWER(C.smsName) LIKE LOWER(concat('%', concat(?1, '%')))")
    List<ContactSms> findBySmsNameLike(String smsName);

}
