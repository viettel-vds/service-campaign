package com.viettel.campaign.repository.ccms_full.impl;

import com.viettel.campaign.config.DataSourceQualify;

import com.viettel.campaign.model.ccms_full.CampaignMessager;
import com.viettel.campaign.repository.ccms_full.CampaignMessagerRepositoryCustom;
import com.viettel.campaign.utils.Constants;
import com.viettel.campaign.utils.HibernateUtil;
import com.viettel.campaign.utils.SQLBuilder;
import com.viettel.campaign.web.dto.ApiResultPagingDTO;
import com.viettel.campaign.web.dto.CampaignDTO;
import com.viettel.campaign.web.dto.CampaignMessagerDTO;
import com.viettel.campaign.web.dto.ResultDTO;
import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.text.Format;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

@Repository
public class CampaignMessagerRepositoryImpl implements CampaignMessagerRepositoryCustom {
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    Session session = null;
    @Autowired
    @Qualifier(DataSourceQualify.NAMED_JDBC_PARAMETER_TEMPLATE_CCMS_FULL)
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public ResultDTO getCampaignMessagerList(CampaignMessagerDTO dto) {
        ResultDTO resultDTO = new ResultDTO();
        List<CampaignMessagerDTO> list = new ArrayList<>();
        Map<String, String> params = new HashMap<>();
        StringBuilder stringBuilder = new StringBuilder();
        session = sessionFactory.openSession();
        session.beginTransaction();

        stringBuilder.append("		SELECT");
        stringBuilder.append("			messager_id messagerId,");
        stringBuilder.append("			messager_title messagerTitle,");
        stringBuilder.append("			messager_customer messagerCustomer,");
        stringBuilder.append("			mesager_content mesagerContent,");
        stringBuilder.append("          campain_id campaignId,");
        stringBuilder.append("			start_date startDate,");
        stringBuilder.append("			end_date startDate");
        stringBuilder.append("        FROM");
        stringBuilder.append("                campaign_messager");
        stringBuilder.append("        WHERE campaign_messager.IS_DELETE!=1 ");
        stringBuilder.append("          AND  ( "+ dto.getCampaignCode() +" IS NULL");
        stringBuilder.append("			OR campaign_messager.campaign_code = "+ dto.getCampaignCode() +" ) ");
        stringBuilder.append("          AND  ( "+ dto.getMessagerId() +" IS NULL");
        stringBuilder.append("			OR campaign_messager.messager_id = "+ dto.getMessagerId() +" ) ");
        stringBuilder.append("			AND ( '"+ dto.getMessagerTitle() +"' IS NULL");
        stringBuilder.append("			OR campaign_messager.messager_title LIKE concat('%', concat('" + dto.getMessagerTitle() +"', '%')) )");
//        stringBuilder.append("			AND ( TO_DATE('"+java.time.LocalDateTime.now()+"','dd-mm-yyyy') IS NULL");
//        stringBuilder.append("			OR campaign_messager.start_date >= trunc(TO_DATE('"+java.time.LocalDateTime.now()+"','dd-mm-yyyy'), 'DD') )");
//        stringBuilder.append("			AND ( '"+dto.getEndDate()+"' IS NULL");
//        stringBuilder.append("			OR campaign_messager.end_date < trunc('"+dto.getEndDate()+"', 'DD') + 1 )");
        SQLQuery query = session.createSQLQuery(stringBuilder.toString());

        query.addScalar("messagerId", new LongType());
        query.addScalar("messagerTitle", new StringType());
        query.addScalar("messagerCustomer", new StringType());
        query.addScalar("mesagerContent", new StringType());
        query.addScalar("campaignId", new LongType());
        query.addScalar("startDate", new DateType());
        query.addScalar("startDate", new DateType());
        query.setResultTransformer(Transformers.aliasToBean(CampaignMessagerDTO.class));
        List<CampaignMessagerDTO> CampaignMessagerDTOList = query.list();
        resultDTO.setListData(CampaignMessagerDTOList);
        resultDTO.setErrorCode(Constants.ApiErrorCode.SUCCESS);
        resultDTO.setDescription(Constants.ApiErrorDesc.SUCCESS);

        return resultDTO;
    }

    @Override
    public ResultDTO getCampaignMessagerListExt(Integer currentPage, Integer perPage,CampaignMessagerDTO requestDto) {
        ResultDTO result = new ResultDTO();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

        if (perPage==null){
            perPage=10;
        }
        if (currentPage==null){
            currentPage=1;
        }
        Integer StartRow=(currentPage - 1) * perPage;
        try {
            StringBuilder stringBuilder = new StringBuilder();
            session = sessionFactory.openSession();
            session.beginTransaction();

            stringBuilder.append("		SELECT * FROM (");
            stringBuilder.append("			SELECT ROWNUM NUM, messager_id messagerId,");
            stringBuilder.append("			messager_code messagerCode,");
            stringBuilder.append("			messager_title messagerTitle,");
            stringBuilder.append("			messager_customer messagerCustomer,");
            stringBuilder.append("			mesager_content mesagerContent,");
            stringBuilder.append("          TO_TIMESTAMP ((messager_date + ( "+requestDto.getTimezoneOffset()+" / 60 ) / 24),'DD-MON-RRHH24:MI:SS.FF') messagerDate, ");
            stringBuilder.append("          campaign_code campaignCode,");
            stringBuilder.append("          campaign_name campaignName,");
            stringBuilder.append("			is_delete isDelete");
            stringBuilder.append("        FROM");
            stringBuilder.append("                campaign_messager");
            stringBuilder.append("        WHERE (campaign_messager.is_delete IS NULL OR campaign_messager.is_delete = 0) ");
            stringBuilder.append("        AND campaign_messager.campaign_code = '"+ requestDto.getCampaignCode()+"'");
            if(requestDto.getMessagerCode()!=null&&requestDto.getMessagerCode()!=""){
                stringBuilder.append("			AND campaign_messager.messager_code LIKE concat('%', concat('" + requestDto.getMessagerCode() +"', '%'))");
            }
            if(requestDto.getMessagerTitle()!=null&&requestDto.getMessagerTitle()!=""){
                stringBuilder.append("			AND campaign_messager.messager_title_ext LIKE concat('%', concat('" + removeAccent(requestDto.getMessagerTitle().toLowerCase()) +"', '%'))");
            }
            if(requestDto.getStartDate()!=null){
                String startDate= formatter.format(requestDto.getStartDate());
                stringBuilder.append("			AND campaign_messager.messager_date >= trunc(TO_DATE('"+startDate+"','dd-MM-yyyy'), 'DD') ");
            }
            if(requestDto.getEndDate()!=null){
                String endDate= formatter.format(requestDto.getEndDate());
                stringBuilder.append("			AND campaign_messager.messager_date < trunc(TO_DATE('"+endDate+"','dd-MM-yyyy'), 'DD')  + 1 ");
            }
            stringBuilder.append("	ORDER BY messager_id desc		)");
            stringBuilder.append("	WHERE NUM BETWEEN " + StartRow + " AND " + Integer.valueOf(perPage + StartRow));
            //stringBuilder.append("	OFFSET "+ StartRow +" ROWS FETCH NEXT "+perPage+" ROWS ONLY	");

            SQLQuery query = session.createSQLQuery(stringBuilder.toString());
            query.addScalar("messagerId", new LongType());
            query.addScalar("messagerCode", new StringType());
            query.addScalar("messagerTitle", new StringType());
            query.addScalar("messagerCustomer", new StringType());
            query.addScalar("mesagerContent", new StringType());
            query.addScalar("messagerDate", new TimestampType());
            query.addScalar("campaignCode", new StringType());
            query.addScalar("campaignName", new StringType());
            query.addScalar("isDelete", new BooleanType());
            query.setResultTransformer(Transformers.aliasToBean(CampaignMessagerDTO.class));

            int total = getCampaignMessagerListCount(requestDto);
            List<CampaignMessagerDTO> list = query.list();
            ApiResultPagingDTO apiResult=new ApiResultPagingDTO();
            apiResult.setTotal(total);
            apiResult.setPerPage(perPage);
            apiResult.setCurrentPage(currentPage);
            apiResult.setLastPage((int)Math.ceil(total / (double)perPage));
            apiResult.setApiResult(list);

            result.setData(apiResult);
            result.setErrorCode(Constants.ApiErrorCode.SUCCESS);
            result.setDescription(Constants.ApiErrorDesc.SUCCESS);
        }
        catch (Exception ex) {
            result.setErrorCode(Constants.ApiErrorCode.ERROR);
            result.setDescription(Constants.ApiErrorDesc.ERROR);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return result;
    }

    @Override
    public ResultDTO getCampaignMessagerLogList(Integer currentPage, Integer perPage, Long messagerId, Integer timezoneOffset) {
        ResultDTO result = new ResultDTO();
        if (perPage==null){
            perPage=10;
        }
        if (currentPage==null){
            currentPage=1;
        }
        Integer StartRow=(currentPage - 1) * perPage;
        try {
            StringBuilder stringBuilder = new StringBuilder();
            session = sessionFactory.openSession();
            session.beginTransaction();

            stringBuilder.append(" SELECT * FROM (");
            stringBuilder.append(" SELECT  ROWNUM NUM, ");
            stringBuilder.append(" messager_id_log messagerIdLog, ");
            stringBuilder.append(" messager_id messagerId, ");
            stringBuilder.append(" messager_title messagerTitle, ");
            stringBuilder.append(" mesager_content mesagerContent, ");
            stringBuilder.append(" messager_customer messagerCustomer, ");
            //stringBuilder.append(" messager_date messagerDate, ");
            stringBuilder.append(" TO_TIMESTAMP ((messager_date + ( "+timezoneOffset+" / 60 ) / 24),'DD-MON-RRHH24:MI:SS.FF') messagerDate, ");
            stringBuilder.append(" messager_code messagerCode, ");
            stringBuilder.append(" campaign_code campaignCode, ");
            stringBuilder.append(" is_delete isDelete, ");
            stringBuilder.append(" messager_title_new messagerTitleNew, ");
            stringBuilder.append(" mesager_content_new mesagerContentNew ");
            stringBuilder.append(" FROM ");
            stringBuilder.append("        campaign_messager_log ");
            stringBuilder.append(" WHERE ");
            stringBuilder.append(" campaign_messager_log.is_delete IS NULL ");
            stringBuilder.append(" OR campaign_messager_log.is_delete = 0 ");
            if(messagerId!=null||messagerId!=0){
                stringBuilder.append(" AND campaign_messager_log.messager_id = "+messagerId);
            }
            stringBuilder.append("	ORDER BY messager_id desc		)");
            stringBuilder.append("	WHERE NUM BETWEEN " + StartRow + " AND " + Integer.valueOf(perPage + StartRow));

            SQLQuery query = session.createSQLQuery(stringBuilder.toString());
            query.addScalar("messagerIdLog", new LongType());
            query.addScalar("messagerId", new LongType());
            query.addScalar("messagerTitle", new StringType());
            query.addScalar("mesagerContent", new StringType());
            query.addScalar("messagerTitleNew", new StringType());
            query.addScalar("mesagerContentNew", new StringType());
            query.addScalar("messagerCustomer", new StringType());
            query.addScalar("messagerDate", new TimestampType());
            query.addScalar("messagerCode", new StringType());
            query.addScalar("campaignCode", new StringType());
            query.addScalar("isDelete", new BooleanType());

            query.setResultTransformer(Transformers.aliasToBean(CampaignMessagerDTO.class));

            int total = getCampaignMessagerLogListCount(messagerId);
            List<CampaignMessagerDTO> list = query.list();
            ApiResultPagingDTO apiResult=new ApiResultPagingDTO();
            apiResult.setTotal(total);
            apiResult.setPerPage(perPage);
            apiResult.setCurrentPage(currentPage);
            apiResult.setLastPage((int)Math.ceil(total / (double)perPage));
            apiResult.setApiResult(list);

            result.setData(apiResult);
            result.setErrorCode(Constants.ApiErrorCode.SUCCESS);
            result.setDescription(Constants.ApiErrorDesc.SUCCESS);
        }
        catch (Exception ex) {
            result.setErrorCode(Constants.ApiErrorCode.ERROR);
            result.setDescription(Constants.ApiErrorDesc.ERROR);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return result;
    }

    private int getCampaignMessagerListCount(CampaignMessagerDTO requestDto){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        StringBuilder stringBuilder = new StringBuilder();
        session = sessionFactory.openSession();
        session.beginTransaction();

        stringBuilder.append("		SELECT");
        stringBuilder.append("			count(1) as campaigncount");
        stringBuilder.append("        FROM");
        stringBuilder.append("                campaign_messager");
        stringBuilder.append("        WHERE (campaign_messager.is_delete IS NULL OR campaign_messager.is_delete = 0) ");
        stringBuilder.append("        AND campaign_messager.campaign_code = '"+ requestDto.getCampaignCode()+"'");
        if(requestDto.getMessagerCode()!=null&&requestDto.getMessagerCode()!=""){
            stringBuilder.append("			AND campaign_messager.messager_code LIKE concat('%', concat('" + requestDto.getMessagerCode() +"', '%'))");
        }
        if(requestDto.getMessagerTitle()!=null&&requestDto.getMessagerTitle()!=""){
            stringBuilder.append("			AND campaign_messager.messager_title_ext LIKE concat('%', concat('" + removeAccent(requestDto.getMessagerTitle().toLowerCase()) +"', '%'))");
        }
        if(requestDto.getStartDate()!=null){
            String startDate= formatter.format(requestDto.getStartDate());
            stringBuilder.append("			AND campaign_messager.messager_date >= trunc(TO_DATE('"+startDate+"','dd-MM-yyyy'), 'DD') ");
        }
        if(requestDto.getEndDate()!=null){
            String endDate= formatter.format(requestDto.getEndDate());
            stringBuilder.append("			AND campaign_messager.messager_date < trunc(TO_DATE('"+endDate+"','dd-MM-yyyy'), 'DD')  + 1 ");
        }

        SQLQuery query = session.createSQLQuery(stringBuilder.toString());
        query.addScalar("campaigncount", new IntegerType());
        query.setResultTransformer(Transformers.aliasToBean(CampaignMessagerDTO.class));

        CampaignMessagerDTO total = (CampaignMessagerDTO) query.uniqueResult();
        return  total.getCampaigncount();
    }

    private int getCampaignMessagerLogListCount(Long messagerId){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        StringBuilder stringBuilder = new StringBuilder();
        session = sessionFactory.openSession();
        session.beginTransaction();

        stringBuilder.append(" SELECT ");
        stringBuilder.append("			count(1) as campaigncount");
        stringBuilder.append(" FROM ");
        stringBuilder.append("        campaign_messager_log ");
        stringBuilder.append(" WHERE ");
        stringBuilder.append(" campaign_messager_log.is_delete IS NULL ");
        stringBuilder.append(" OR campaign_messager_log.is_delete = 0 ");
        if(messagerId!=null||messagerId!=0){
            stringBuilder.append(" AND campaign_messager_log.messager_id = "+messagerId);
        }
        SQLQuery query = session.createSQLQuery(stringBuilder.toString());
        query.addScalar("campaigncount", new IntegerType());
        query.setResultTransformer(Transformers.aliasToBean(CampaignMessagerDTO.class));

        CampaignMessagerDTO total = (CampaignMessagerDTO) query.uniqueResult();
        return  total.getCampaigncount();
    }
    @Override
    public String getLastID(String Prefix) {
        StringBuilder stringBuilder = new StringBuilder();
        session = sessionFactory.openSession();
        session.beginTransaction();

        stringBuilder.append("SELECT messagerCode FROM( SELECT messager_code AS messagercode, messager_id FROM campaign_messager WHERE messager_code LIKE concat('"+Prefix+"', '%') ORDER BY messager_date desc,MESSAGER_ID  DESC) WHERE ROWNUM=1");
        SQLQuery query = session.createSQLQuery(stringBuilder.toString());
        query.addScalar("messagerCode", new StringType());
        query.setResultTransformer(Transformers.aliasToBean(CampaignMessagerDTO.class));
        CampaignMessagerDTO dto = (CampaignMessagerDTO) query.uniqueResult();
        if(dto==null){return null;}
        return dto.getMessagerCode();
    }

    @Override
    public boolean InsertMessager(CampaignMessager item) {
        StringBuilder stringBuilder = new StringBuilder();
        session = sessionFactory.openSession();
        session.beginTransaction();

        try{
            stringBuilder.append(" INSERT INTO campaign_messager ( ");
            stringBuilder.append("        messager_title, ");
            stringBuilder.append("        mesager_content, ");
            stringBuilder.append("        messager_customer, ");
            stringBuilder.append("        messager_code, ");
            stringBuilder.append("        campaign_code, ");
            stringBuilder.append("        campaign_name ");
            stringBuilder.append(" ) VALUES ( ");
            stringBuilder.append("        '"+item.getMessagerTitle()+"', ");
            stringBuilder.append("        '"+item.getMesagerContent()+"', ");
            stringBuilder.append("        '"+item.getMessagerCustomer()+"', ");
            stringBuilder.append("        '"+item.getMessagerCode()+"', ");
            stringBuilder.append("        '"+item.getCampaignCode()+"', ");
            stringBuilder.append("        '"+item.getCampaignName()+"' ");
            stringBuilder.append(" ) ");
            SQLQuery query = session.createSQLQuery(stringBuilder.toString());
            query.executeUpdate();
        }
        catch (Exception ex) {
            return false;
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return  true;
    }

    @Override
    public CampaignMessagerDTO GetMessagerInId(String messagerCode) {
        StringBuilder stringBuilder = new StringBuilder();
        session = sessionFactory.openSession();
        session.beginTransaction();

        stringBuilder.append("		SELECT");
        stringBuilder.append("			messager_id messagerId,");
        stringBuilder.append("			messager_code messagerCode,");
        stringBuilder.append("			messager_title messagerTitle,");
        stringBuilder.append("			messager_customer messagerCustomer,");
        stringBuilder.append("			mesager_content mesagerContent,");
        stringBuilder.append("          messager_date messagerDate,");
        stringBuilder.append("          campaign_code campaignCode,");
        stringBuilder.append("          campaign_name campaignName,");
        stringBuilder.append("			is_delete isDelete");
        stringBuilder.append("        FROM");
        stringBuilder.append("                campaign_messager");
        stringBuilder.append("        WHERE  messager_code='"+messagerCode+"' ");

        SQLQuery query = session.createSQLQuery(stringBuilder.toString());
        query.addScalar("messagerId", new LongType());
        query.addScalar("messagerCode", new StringType());
        query.addScalar("messagerTitle", new StringType());
        query.addScalar("messagerCustomer", new StringType());
        query.addScalar("mesagerContent", new StringType());
        query.addScalar("messagerDate", new DateType());
        query.addScalar("campaignCode", new StringType());
        query.addScalar("campaignName", new StringType());
        query.addScalar("isDelete", new BooleanType());
        query.setResultTransformer(Transformers.aliasToBean(CampaignMessagerDTO.class));
        CampaignMessagerDTO dto = (CampaignMessagerDTO) query.uniqueResult();
        if(dto==null){return null;}
        return dto;
    }

    public static String removeAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }
}

