package com.viettel.campaign.repository.ccms_full.impl;

import com.viettel.campaign.model.ccms_full.Calendar;
import com.viettel.campaign.model.ccms_full.CalendarHoliday;
import com.viettel.campaign.model.ccms_full.CalendarTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CalendarRepository extends JpaRepository<Calendar, Long> {

    @Query(value = "SELECT * FROM CALENDAR WHERE SITE_ID = ?1 AND STATUS = 1 AND CHANNEL_ID = 7 ", nativeQuery = true)
    List<Calendar> findAllBySiteId(Integer siteId);
}
