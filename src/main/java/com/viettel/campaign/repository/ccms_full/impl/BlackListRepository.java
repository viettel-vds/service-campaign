package com.viettel.campaign.repository.ccms_full.impl;

import com.viettel.campaign.model.ccms_full.BlackList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlackListRepository extends JpaRepository<BlackList, Long> {

    List<BlackList> findAllByStatus(Long status);
}
