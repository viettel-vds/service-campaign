package com.viettel.campaign.repository.ccms_full;

import com.viettel.campaign.web.dto.*;
import com.viettel.campaign.web.dto.request_dto.BlacklistRequestDTO;
import com.viettel.campaign.web.dto.request_dto.CustomerDetailRequestDTO;
import com.viettel.campaign.web.dto.request_dto.SeachCustomerDTO;
import com.viettel.campaign.web.dto.request_dto.SearchCustomerRequestDTO;
import com.viettel.campaign.web.dto.ResultDTO;
import com.viettel.econtact.filter.UserSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CustomerRepositoryCustom {

    List<CustomerDetailRequestDTO> getCustomerDetailById(Long companySiteId, Long customerId, Long timezoneOffset);

    Page<CustomerCustomDTO> getAllCustomerByParams(SearchCustomerRequestDTO searchCustomerRequestDTO, Pageable pageable);

    List<CampaignInformationDTO> getCampaignInformation(CampaignCustomerDTO campaignCustomerDTO);

    Page<CustomerCustomDTO> getIndividualCustomerInfo(CampaignCustomerDTO campaignCustomerDTO, Pageable pageable);

    List<CustomerListDTO> getCustomerListInfo(CampaignCustomerDTO campaignCustomerDTO);

    Page<CustomerDTO> getCustomizeFields(CampaignCustomerDTO campaignCustomerDTO, UserSession userSession, Pageable pageable);

    List<CustomerDetailRequestDTO> getIndividualCustomerDetailById(Long companySiteId, Long customerId, Long timezoneOffset);
    ResultDTO loadAllBlacklist();
    ResultDTO ListCampaignInCustomer(Integer currentPage, Integer perPage,CampaignCustomerExtDTO campaignCustomerExtDTO);

    List<CustomerDTO> getCustomerExports(SeachCustomerDTO seachCustomerDTO, Integer pageNo, Integer pageSize);


}
