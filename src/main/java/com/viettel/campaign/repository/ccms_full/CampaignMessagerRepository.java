package com.viettel.campaign.repository.ccms_full;
import com.viettel.campaign.model.ccms_full.CampaignMessager;
import com.viettel.campaign.model.ccms_full.CustomizeFields;
import com.viettel.campaign.web.dto.CampaignMessagerDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.viettel.campaign.web.dto.ResultDTO;

import java.util.Date;
import java.util.List;

@Repository
public interface CampaignMessagerRepository extends JpaRepository<CampaignMessager, Long> , CampaignMessagerRepositoryCustom{
    // @Override
    //List<CampaignMessager> findAll();

    CampaignMessager findByMessagerId(Long id);

    //@Override
    //ResultDTO getCampaignMessagerList(CampaignMessagerDTO dto);
}
