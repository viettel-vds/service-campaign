package com.viettel.campaign.repository.ccms_full.impl;

import com.viettel.campaign.model.ccms_full.CalendarHoliday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CalendarHolidayRepository extends JpaRepository<CalendarHoliday, Long> {

    @Query(value = "SELECT * FROM CALENDAR_HOLIDAY WHERE CALENDAR_ID = ?1 AND STATUS = ?2 AND MONTH >= ?3 AND MONTH <= ?4 ", nativeQuery = true)
    List<CalendarHoliday> findAllByCalendarIdAndStatus(Integer calendarId, int status, int fromMonth, int toMonth);

}
