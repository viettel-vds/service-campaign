package com.viettel.campaign.repository.ccms_full;

import com.viettel.campaign.model.ccms_full.CampaignMessager;
import com.viettel.campaign.model.ccms_full.CampaignMessagerLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CampaignMessagerLogReponsitory extends JpaRepository<CampaignMessagerLog, Long> {
}
