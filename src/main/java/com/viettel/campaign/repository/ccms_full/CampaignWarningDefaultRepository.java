package com.viettel.campaign.repository.ccms_full;

import com.viettel.campaign.model.ccms_full.CampaignWarningDefault;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CampaignWarningDefaultRepository extends JpaRepository<CampaignWarningDefault, Long> {
    Page<CampaignWarningDefault> findAll(Pageable pageable);
}
