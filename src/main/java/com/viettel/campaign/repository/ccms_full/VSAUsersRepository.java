package com.viettel.campaign.repository.ccms_full;

import com.viettel.campaign.model.ccms_full.VSAUsers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VSAUsersRepository extends JpaRepository<VSAUsers, Long> {

    List<VSAUsers> findAllByCompanySiteId(Long companySiteId);

    VSAUsers findByUserId(Long userId);

    @Query(value= "select u from VSAUsers u join UserRole ur on u.userId = ur.userId and ur.roleId <> 3 and u.companySiteId = :companySiteId ")
    List<VSAUsers> findUserCreatedList(@Param("companySiteId") Long companySiteId);
}
