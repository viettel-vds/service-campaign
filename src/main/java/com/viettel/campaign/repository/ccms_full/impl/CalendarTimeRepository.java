package com.viettel.campaign.repository.ccms_full.impl;

import com.viettel.campaign.model.ccms_full.Calendar;
import com.viettel.campaign.model.ccms_full.CalendarTime;
import com.viettel.campaign.model.ccms_full.CampaignCfg;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Repository
public interface CalendarTimeRepository extends JpaRepository<CalendarTime, Long> {

    @Query(value = "SELECT * FROM CALENDAR_TIME WHERE CALENDAR_ID = ?1 AND STATUS = ?2", nativeQuery = true)
    List<CalendarTime> findAllByCalendarIdAndStatus(Integer calendarId, int status);

    @Query(value = "SELECT * FROM CALENDAR_TIME WHERE CALENDAR_ID = ?1 AND STATUS = 1 AND DAY IS NOT NULL ", nativeQuery = true)
    List<CalendarTime> findAllByCalendarIdAndDay(Integer calendarId);

    @Query(value = "SELECT * FROM CALENDAR_TIME WHERE CALENDAR_ID = ?1 AND STATUS = ?2 AND DAY = ?3 ", nativeQuery = true)
    CalendarTime findAllByCalendarIdAndStatusAndDay(Integer calendarId, int status, int day);
}
