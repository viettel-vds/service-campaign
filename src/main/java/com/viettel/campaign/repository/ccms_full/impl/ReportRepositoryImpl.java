package com.viettel.campaign.repository.ccms_full.impl;

import com.viettel.campaign.config.DataSourceProperties;
import com.viettel.campaign.config.DataSourceQualify;
import com.viettel.campaign.model.ccms_full.Campaign;
import com.viettel.campaign.repository.ccms_full.CampaignRepository;
import com.viettel.campaign.repository.ccms_full.ReportRepositoryCustom;
import com.viettel.campaign.utils.BundleUtils;
import com.viettel.campaign.utils.WorkBookBuilder;
import com.viettel.campaign.web.dto.CampaignDTO;
import com.viettel.campaign.web.dto.ReportColDTO;
import com.viettel.campaign.web.dto.ReportQuestionAnswerDTO;
import com.viettel.campaign.web.dto.ReportStatusDTO;
import com.viettel.campaign.web.dto.report_dto.*;
import com.viettel.econtact.filter.UserSession;
import javafx.util.converter.BigDecimalStringConverter;
import javafx.util.converter.LocalDateStringConverter;
import oracle.jdbc.OracleTypes;
import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.core.env.Environment;
import javax.persistence.*;
import java.sql.*;
import java.sql.Date;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
@Repository
public class ReportRepositoryImpl implements ReportRepositoryCustom {
    private UserSession userSession;
    @PersistenceContext(unitName = DataSourceQualify.JPA_UNIT_NAME_CCMS_FULL)
    private EntityManager entityManager;

    private final String FORMAT_DATE_YYYY = "yyyy-MM-dd";
    @Override
    public int getTotal(String queryString, HashMap<String, Object> hmapParams) {
        return 0;
    }
    // export excel
    private CellStyle setBorder(CellStyle style, BorderStyle borderStyle){
        style.setBorderBottom(borderStyle);
        style.setBorderLeft(borderStyle);
        style.setBorderRight(borderStyle);
        style.setBorderTop(borderStyle);
        return style;
    }
    private void setBorderRegion(BorderStyle borderStyle, CellRangeAddress region, Sheet sheet){
        RegionUtil.setBorderBottom(borderStyle, region, sheet);
        RegionUtil.setBorderLeft(borderStyle, region, sheet);
        RegionUtil.setBorderTop(borderStyle, region, sheet);
        RegionUtil.setBorderRight(borderStyle, region, sheet);
    }
    public XSSFWorkbook exportReportGeneral(HashMap<String, Object> hmapParams){
        XSSFWorkbook workbook = new XSSFWorkbook();
        List<ReportQuestionAnswerDTO> listData = getStatistic(hmapParams);
        List<ReportQuestionAnswerDTO> listDataQuestion = getQuestionAnswer2(hmapParams);
        Sheet sheet;
        // style
        CellStyle styleTitle = WorkBookBuilder.buildDefaultStyleTitle(workbook);
        CellStyle styleRowHeader = WorkBookBuilder.buildDefaultStyleRowHeader(workbook);
        CellStyle styleRow = WorkBookBuilder.buildDefaultStyleRow(workbook);
        CellStyle styleProperties = WorkBookBuilder.buildDefaultStyleRow(workbook);
        CellStyle styleNumber = WorkBookBuilder.buildDefaultStyleRow(workbook);
        CellStyle styleRowTotal = styleRowHeader;
        CellStyle styleQuestion = WorkBookBuilder.buildDefaultStyleRow(workbook);
        // midle alight
        styleQuestion.setVerticalAlignment(VerticalAlignment.CENTER);

        styleTitle.setAlignment(HorizontalAlignment.CENTER);
        styleNumber.setAlignment(HorizontalAlignment.CENTER);
        styleRowHeader.setWrapText(true);
        styleRow.setWrapText(true);
        // set border
        styleRow = setBorder(styleRow, BorderStyle.THIN);
        styleQuestion = setBorder(styleQuestion, BorderStyle.THIN);
        styleNumber = setBorder(styleNumber, BorderStyle.THIN);
        // list header
        List<String> fileHeaderList = new ArrayList<>();
        fileHeaderList.add("Câu hỏi");
        fileHeaderList.add("STT");
        fileHeaderList.add("Lựa chọn");
        fileHeaderList.add("Số lượng");
        fileHeaderList.add("Tỷ lệ");

        sheet = workbook.createSheet("sheet 1");
        // Header
        int startRowTable = 5;
        int count = 1;
        Row rowHeader = sheet.createRow(startRowTable);
        for (int i = 0; i < fileHeaderList.size(); i++) {

            WorkBookBuilder.writeCellContent(rowHeader, styleRowHeader, i, fileHeaderList.get(i));
        }
        sheet.setColumnWidth(0, 8000);
        sheet.setColumnWidth(1, 2000);
        sheet.setColumnWidth(2, 21000);
        sheet.setColumnWidth(3, 4000);
        sheet.setColumnWidth(4, 4000);
        // group by theo cau hoi
        HashMap<String, GroupQuestion> hashMapTKKN = convertGroupQuestion(listData);
        HashMap<String, GroupQuestion> hashMapQuestion = convertGroupQuestion(listDataQuestion);
        HashMap<String, Long> hashMapQuestionOrderIndex = convertGroupQuestionOrderIndex(listDataQuestion);// dung de sort
        // Content thống kê kết nối
        for ( String key : hashMapTKKN.keySet() ) {
            GroupQuestion groupQuestion = hashMapTKKN.get(key);
            int col = 0;
            int firstRow = startRowTable + count;// dùng để merge ô câu hỏi
            float totalpercent =0f;
            // fetch câu trả lời
            for (int i = 0,rowIndex=1; i < groupQuestion.listAnswer.size(); i++){
                Row rowA = sheet.createRow(startRowTable + count);
                if(i==0){
                    WorkBookBuilder.writeCellContent(rowA, styleQuestion, col, key);
                }
                WorkBookBuilder.writeCellContent(rowA, styleNumber, col+1, rowIndex);
                WorkBookBuilder.writeCellContent(rowA, styleRow, col+2, groupQuestion.listAnswer.get(i).getAnswer());
                WorkBookBuilder.writeCellContent(rowA, styleNumber, col+3, groupQuestion.listAnswer.get(i).getAnswerCount());
                // trường hợp tổng =0 chia bị lỗi
                Float percentCount=0f;
                if(groupQuestion.totalCount !=0){
                    percentCount = ((float)groupQuestion.listAnswer.get(i).getAnswerCount()/(float)groupQuestion.totalCount);
                }
                NumberFormat nf = NumberFormat.getPercentInstance();
                nf.setMaximumFractionDigits(2);
                WorkBookBuilder.writeCellContent(rowA, styleNumber, col+4, nf.format(percentCount));
                rowIndex++;
                count++;
            }
            // dòng tính tổng mỗi câu hỏi
            Row rowTotal = sheet.createRow(startRowTable + count);
            WorkBookBuilder.writeCellContent(rowTotal, styleRowTotal, col+1, "");
            WorkBookBuilder.writeCellContent(rowTotal, styleRowTotal, col+2, "Tổng");
            WorkBookBuilder.writeCellContent(rowTotal, styleRowTotal, col+3, groupQuestion.totalCount);
            WorkBookBuilder.writeCellContent(rowTotal, styleRowTotal, col+4, "100%");
            CellRangeAddress region = new CellRangeAddress(firstRow, startRowTable + count, 0, 0);
            sheet.addMergedRegion(region);
            setBorderRegion(BorderStyle.THIN, region, sheet);
            count++;
        }
        // Content câu hỏi
        List<String> answerGroupByKey = new ArrayList<>(hashMapQuestion.keySet());
        //Collections.sort(answerGroupByKey); // sort asc
        Collections.sort(answerGroupByKey,(s1,s2) -> {
            return (int) (hashMapQuestionOrderIndex.get(s1) - hashMapQuestionOrderIndex.get(s2));
        });
        for ( String key : answerGroupByKey ) {
            GroupQuestion groupQuestion = hashMapQuestion.get(key);
            int col = 0;
            int firstRow = startRowTable + count;// dùng để merge ô câu hỏi
            float totalpercent =0f;
            // fetch câu trả lời
            for (int i = 0,rowIndex=1; i < groupQuestion.listAnswer.size(); i++){
                Row rowA = sheet.createRow(startRowTable + count);
                if(i==0){
                    WorkBookBuilder.writeCellContent(rowA, styleQuestion, col, key);
                }
                WorkBookBuilder.writeCellContent(rowA, styleNumber, col+1, rowIndex);
                WorkBookBuilder.writeCellContent(rowA, styleRow, col+2, groupQuestion.listAnswer.get(i).getAnswer());
                WorkBookBuilder.writeCellContent(rowA, styleNumber, col+3, groupQuestion.listAnswer.get(i).getAnswerCount());
                // trường hợp tổng =0 chia bị lỗi
                Float percentCount=0f;
                if(groupQuestion.totalCount !=0){
                    percentCount = ((float)groupQuestion.listAnswer.get(i).getAnswerCount()/(float)groupQuestion.totalCount);
                }
                NumberFormat nf = NumberFormat.getPercentInstance();
                nf.setMaximumFractionDigits(2);
                WorkBookBuilder.writeCellContent(rowA, styleNumber, col+4, nf.format(percentCount));
                rowIndex++;
                count++;
            }
            // dòng tính tổng mỗi câu hỏi
            Row rowTotal = sheet.createRow(startRowTable + count);
            WorkBookBuilder.writeCellContent(rowTotal, styleRowTotal, col+1, "");
            WorkBookBuilder.writeCellContent(rowTotal, styleRowTotal, col+2, "Tổng");
            WorkBookBuilder.writeCellContent(rowTotal, styleRowTotal, col+3, groupQuestion.totalCount);
            WorkBookBuilder.writeCellContent(rowTotal, styleRowTotal, col+4, "100%");
            CellRangeAddress region = new CellRangeAddress(firstRow, startRowTable + count, 0, 0);
            sheet.addMergedRegion(region);
            setBorderRegion(BorderStyle.THIN, region, sheet);
            count++;
        }
        // tiêu đề
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4));
        //WorkBookBuilder.writeCellContent(sheet.createRow(0), styleNumber, 0, "Báo cáo tổng hợp số liệu");
        // tiêu đề báo cáo
        String campaignName = getByCampaignNameByCode(hmapParams.get("campaignCode").toString());
        String titleReport = "Báo cáo tổng hợp số liệu";
        if(hmapParams.get("title_report") != null){
            titleReport = hmapParams.get("title_report").toString() + ": " + campaignName;
        }
        WorkBookBuilder.writeCellContent(sheet.createRow(0), styleRowHeader, 0, titleReport);

        // các tiêu chí
        Row properties1 = sheet.createRow(1);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        WorkBookBuilder.writeCellContent(properties1, styleProperties, 0, "Thời điểm xuất báo cáo ");
        WorkBookBuilder.writeCellContent(properties1, styleProperties, 1, sdf.format(getCurrentDateByTimezone(hmapParams)));

        Row properties2 = sheet.createRow(2);
        WorkBookBuilder.writeCellContent(properties2, styleProperties, 0, "Mã chiến dịch ");
        WorkBookBuilder.writeCellContent(properties2, styleProperties, 1, hmapParams.get("campaignCode").toString());

        Row properties3 = sheet.createRow(3);
        WorkBookBuilder.writeCellContent(properties3, styleProperties, 0, "Tài khoản ");
        WorkBookBuilder.writeCellContent(properties3, styleProperties, 1, userSession.getUserName());
        return workbook;
    }
    public XSSFWorkbook exportReportDeatail(HashMap<String, Object> hmapParams){
        XSSFWorkbook workbook = new XSSFWorkbook();
        List<ReportDetailDTO> listDetail = getDetail(hmapParams);
        List<ReportCusInfoDTO> listCusInfo = getCusInfo(hmapParams);
        List<ReportQuestionAnswerDTO> listQuestionAnswer = getQuestionAnswer(hmapParams);

        // group by ten trang thai
        HashMap<String, List<ReportCusInfoDTO>> cusInfoGroup = convertCusInfoGroupByTitle(listCusInfo);
        HashMap<String, GroupQuestion> answerGroupByQuestion = convertGroupQuestion(listQuestionAnswer);
        if(cusInfoGroup.size() < 1){
            cusInfoGroup.put("", new ArrayList<>());
            cusInfoGroup.put(" ", new ArrayList<>());
        }
        if(answerGroupByQuestion.size() < 1){
            //answerGroupByQuestion.put("", new GroupQuestion());
            //answerGroupByQuestion.put(" ", new GroupQuestion());
        }
        Sheet sheet;
        // style
        CellStyle styleTitle = WorkBookBuilder.buildDefaultStyleTitle(workbook);
        CellStyle styleRowHeader = WorkBookBuilder.buildDefaultStyleRowHeader(workbook);
        CellStyle styleRow = WorkBookBuilder.buildDefaultStyleRow(workbook);
        CellStyle styleCenter = WorkBookBuilder.buildDefaultStyleRow(workbook);
        CellStyle styleRowTotal = styleRowHeader;
        CellStyle styleProperties = WorkBookBuilder.buildDefaultStyleRow(workbook);
        CellStyle styleQuestion = WorkBookBuilder.buildDefaultStyleRow(workbook);
        // midle alight
        styleQuestion.setVerticalAlignment(VerticalAlignment.CENTER);

        styleTitle.setAlignment(HorizontalAlignment.CENTER);
        //styleTitle.setFillBackgroundColor(HSSFColor.toHSSFColor(new ));
        styleCenter.setAlignment(HorizontalAlignment.CENTER);
        styleCenter.setVerticalAlignment(VerticalAlignment.CENTER);
        styleRowHeader.setWrapText(true);
        styleRowHeader.setVerticalAlignment(VerticalAlignment.CENTER);
        styleRow.setWrapText(true);
        styleCenter.setWrapText(true);
        // border
        styleRow = setBorder(styleRow, BorderStyle.THIN);
        styleCenter = setBorder(styleCenter, BorderStyle.THIN);
        // list header
        List<String> fileHeaderList = new ArrayList<>();
        fileHeaderList.add("STT");
        fileHeaderList.add("Tên chiến dịch");
        fileHeaderList.add("Mã chiến dịch");
        fileHeaderList.add("Thời gian thực hiện");
        fileHeaderList.add("Số điện thoại");
        fileHeaderList.add("Tên khách hàng");
        // thông tin cho tư vấn viên
        List<String> listKeyCusInfo = new ArrayList<>(cusInfoGroup.keySet());
        Collections.sort(listKeyCusInfo);
        for (String key: listKeyCusInfo) {
            fileHeaderList.add(key);
        }
        fileHeaderList.add("Thời gian tương tác lần 1");
        fileHeaderList.add("Độ dài cuộc gọi");
        fileHeaderList.add("Tư vấn viên thực hiện");
        fileHeaderList.add("Trạng thái kết nối");
        fileHeaderList.add("GHI CHÚ");
        // các câu hỏi
        HashMap<String, Long> hashMapQuestionOrderIndex = convertGroupQuestionOrderIndex(listQuestionAnswer);// dung de sort
        List<String> answerGroupByKey = new ArrayList<>(answerGroupByQuestion.keySet());
        //Collections.sort(answerGroupByKey); // sort asc
        try {
            Collections.sort(answerGroupByKey,(s1,s2) -> { // sort by order_index
                return (int) (hashMapQuestionOrderIndex.get(s1) - hashMapQuestionOrderIndex.get(s2));
            });
        } catch (Exception e){
        }

        for(String key: answerGroupByKey){
            fileHeaderList.add(key);
        }
        sheet = workbook.createSheet("sheet 1");
        // Header
        int startRowTable = 6;
        int count = 0;
        int colCurrent=0;
        int numberOfColStatic1=6;
        int numberOfColStatic2=5;
        // cot tinh
        Row rowHeader = sheet.createRow(startRowTable+1);
        Row rowHeader0 = sheet.createRow(startRowTable); // chứa gộp title
        // MERGE CÁC Ô TRÊN TITLE
        if(cusInfoGroup.size() > 0){
            CellRangeAddress region = new CellRangeAddress(startRowTable, startRowTable, numberOfColStatic1, numberOfColStatic1+cusInfoGroup.size()-1);
            sheet.addMergedRegion(region);
            setBorderRegion(BorderStyle.THIN, region, sheet);
        }
        WorkBookBuilder.writeCellContent(rowHeader0, styleRowHeader, numberOfColStatic1, "Các trường thông tin hiển thị cho tư vấn viên trong chiến dịch");
//        if(answerGroupByQuestion.size() > 0){
//            CellRangeAddress region = new CellRangeAddress(startRowTable, startRowTable,
//                    numberOfColStatic1+cusInfoGroup.size()+numberOfColStatic2,
//                    numberOfColStatic1+cusInfoGroup.size()+numberOfColStatic2 +answerGroupByQuestion.size()-1);
//            sheet.addMergedRegion(region);
//            setBorderRegion(BorderStyle.THIN, region, sheet);
//        }
        //WorkBookBuilder.writeCellContent(rowHeader0, styleRowHeader, numberOfColStatic1+cusInfoGroup.size()+numberOfColStatic2, "");
        // tiêu đề 6 ô đầu tĩnh
        for(int i=0;i<numberOfColStatic1;i++){
            CellRangeAddress region = new CellRangeAddress(startRowTable, startRowTable+1, i, i);
            sheet.addMergedRegion(region);
            setBorderRegion(BorderStyle.THIN, region, sheet);
            WorkBookBuilder.writeCellContent(rowHeader0, styleRowHeader, i, fileHeaderList.get(i));
            sheet.setColumnWidth(i, 5000);
            colCurrent++;
        }
        // tiêu đề các thông tin cho tư vấn viên
        for(int i=numberOfColStatic1;i < numberOfColStatic1+cusInfoGroup.size(); i++){
            WorkBookBuilder.writeCellContent(rowHeader, styleRowHeader, i, fileHeaderList.get(i));
            sheet.setColumnWidth(i, 5000);
            colCurrent++;
        }
        // các ô tĩnh tiếp theo
        for(int i=numberOfColStatic1+cusInfoGroup.size(); i<numberOfColStatic1+cusInfoGroup.size()+numberOfColStatic2; i++){
            CellRangeAddress region = new CellRangeAddress(startRowTable, startRowTable+1, i, i);
            sheet.addMergedRegion(region);
            setBorderRegion(BorderStyle.THIN, region, sheet);
            WorkBookBuilder.writeCellContent(rowHeader0, styleRowHeader, i, fileHeaderList.get(i));
            sheet.setColumnWidth(i, 5000);
            colCurrent++;
        }
        // câu hỏi
        for(int i=numberOfColStatic1+cusInfoGroup.size()+numberOfColStatic2;i<numberOfColStatic1+cusInfoGroup.size()+numberOfColStatic2+answerGroupByQuestion.size();i++){
            WorkBookBuilder.writeCellContent(rowHeader0, styleRowHeader, i, fileHeaderList.get(i));
            sheet.setColumnWidth(i, 5000);
            CellRangeAddress region = new CellRangeAddress(rowHeader0.getRowNum(), rowHeader.getRowNum(), i, i);
            sheet.addMergedRegion(region);
            setBorderRegion(BorderStyle.THIN, region, sheet);
            colCurrent++;
        }

        // fill data row
        count+=1;
        int order=0;
        NumberFormat nf = NumberFormat.getPercentInstance();
        nf.setMaximumFractionDigits(2);
        for (ReportDetailDTO row : listDetail) {// vòng lặp các chiến dịch
            int col = 0;
            count += 1;
            order += 1;
            Row rowI = sheet.createRow(startRowTable + count);
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, order);//stt
            WorkBookBuilder.writeCellContent(rowI, styleRow, col++, row.getCampaignName());//ten chiend dich
            WorkBookBuilder.writeCellContent(rowI, styleRow, col++, row.getCampaignCode());//ma
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getStartTime()+" - "+row.getEndTime());//thoi gian bd - kt
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getPhoneNumber());// đếm
            WorkBookBuilder.writeCellContent(rowI, styleRow, col++, row.getName());// đếm
            // cột thông tin khách hàng
            for (String key : listKeyCusInfo) {
                List<ReportCusInfoDTO> lst = cusInfoGroup.get(key);
                if(lst == null) lst = new ArrayList<>();
                // tìm xem có cus_result_id nào trong cột thông tin khách hàng này ko
                boolean existStatus = false;// có tồn tại hay ko
                for (ReportCusInfoDTO rowCusInfo : lst) {
                    if(row.getContactCustResultId().equals(rowCusInfo.getContactCustResultId())){
                        WorkBookBuilder.writeCellContent(rowI, styleRow, col++, rowCusInfo.getValue());
                        existStatus = true;
                        break;
                    }
                }
                if(!existStatus){
                    WorkBookBuilder.writeCellContent(rowI, styleRow, col++, "");
                }
            }
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getStartCall());
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getDuarationCall());
            WorkBookBuilder.writeCellContent(rowI, styleRow, col++, row.getUserName());
            WorkBookBuilder.writeCellContent(rowI, styleRow, col++, row.getCompleteName());
            WorkBookBuilder.writeCellContent(rowI, styleRow, col++, row.getDescription());

            // điền câu trả lời
            for(String key : answerGroupByKey){
                List<ReportQuestionAnswerDTO> listAnswer = answerGroupByQuestion.get(key).listAnswer;
                if(listAnswer == null) listAnswer = new ArrayList<>();
                // tìm xem có cus_result_id nào trong câu hỏi này ko
                boolean existStatus = false;// có tồn tại hay ko
                for (ReportQuestionAnswerDTO rowAnswer : listAnswer) {
                    if(row.getContactCustResultId().equals(rowAnswer.getContactCusResultId())){
//                        Cell cell = rowI.getCell(col);
//                        String cellValue = "";
//                        if(cell != null){
//                            cellValue = cell.getStringCellValue() + ", " + rowAnswer.getAnswer();
//                        } else{
//                            cellValue = rowAnswer.getAnswer();
//                        }
                        WorkBookBuilder.writeCellContent(rowI, styleRow, col, rowAnswer.getAnswer());
                        existStatus = true;
                    }
                }
                if(!existStatus){
                    WorkBookBuilder.writeCellContent(rowI, styleRow, col, "");
                }
                col++;
            }
        }

        // tiêu đề
        CellRangeAddress region = new CellRangeAddress(0, 0, 0, colCurrent-1);
        sheet.addMergedRegion(region);
        setBorderRegion(BorderStyle.THIN, region, sheet);
        //WorkBookBuilder.writeCellContent(sheet.createRow(0), styleCenter, 0, "Báo cáo chi tiết theo chiến dịch");
        // tiêu đề báo cáo
        String titleReport = "Báo cáo chi tiết theo chiến dịch";
        if(hmapParams.get("title_report") != null){
            titleReport = hmapParams.get("title_report").toString();
        }
        WorkBookBuilder.writeCellContent(sheet.createRow(0), styleRowHeader, 0, titleReport);
        // các tiêu chí
        Row properties1 = sheet.createRow(1);
        WorkBookBuilder.writeCellContent(properties1, styleProperties, 0, "Thời điểm xuất báo cáo");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        WorkBookBuilder.writeCellContent(properties1, styleProperties, 2, sdf.format(getCurrentDateByTimezone(hmapParams)));

        Row properties2 = sheet.createRow(2);
        WorkBookBuilder.writeCellContent(properties2, styleProperties, 0, "Mã chiến dịch");
        WorkBookBuilder.writeCellContent(properties2, styleProperties, 1, hmapParams.get("campaignCode").toString());

        Row properties3 = sheet.createRow(3);
        WorkBookBuilder.writeCellContent(properties3, styleProperties, 0, "Báo cáo từ ngày");
        if(hmapParams.get("createTimeFrom") != null){
            WorkBookBuilder.writeCellContent(properties3, styleProperties, 1, formatDateSearch(hmapParams.get("createTimeFrom").toString()));
        }
        if(hmapParams.get("createTimeTo") != null){
            WorkBookBuilder.writeCellContent(properties3, styleProperties, 2, "Đến ngày");
            WorkBookBuilder.writeCellContent(properties3, styleProperties, 3, formatDateSearch(hmapParams.get("createTimeTo").toString()));
        }

        Row properties4 = sheet.createRow(4);
        WorkBookBuilder.writeCellContent(properties4, styleProperties, 0, "Tài khoản");
        WorkBookBuilder.writeCellContent(properties4, styleProperties, 1, userSession.getUserName());
        return workbook;
    }
    public XSSFWorkbook exportReportEffect(HashMap<String, Object> hmapParams){
        XSSFWorkbook workbook = new XSSFWorkbook();
        List<ReportEffectDTO> listEffect = getEffect(hmapParams);
        List<ReportEffectStatusDTO> listStatusContact = getEffectStatus(hmapParams);
        List<ReportEffectStatusDTO> listStatusCall = getEffectStatusCall(hmapParams);

        // group by ten trang thai
        HashMap<String, List<ReportEffectStatusDTO>> statusContactGroup = convertGroupStatus(listStatusContact);
        HashMap<String, List<ReportEffectStatusDTO>> statusCallGroup = convertGroupStatus(listStatusCall);

        Sheet sheet;
        // style
        CellStyle styleTitle = WorkBookBuilder.buildDefaultStyleTitle(workbook);
        CellStyle styleRowHeader = WorkBookBuilder.buildDefaultStyleRowHeader(workbook);
        CellStyle styleRow = WorkBookBuilder.buildDefaultStyleRow(workbook);
        CellStyle styleCenter = WorkBookBuilder.buildDefaultStyleRow(workbook);
        CellStyle styleRowTotal = styleRowHeader;
        CellStyle styleQuestion = WorkBookBuilder.buildDefaultStyleRow(workbook);
        CellStyle styleProperties = WorkBookBuilder.buildDefaultStyleRow(workbook);
        // midle alight
        styleQuestion.setVerticalAlignment(VerticalAlignment.CENTER);

        styleTitle.setAlignment(HorizontalAlignment.CENTER);
        //styleTitle.setFillBackgroundColor(HSSFColor.toHSSFColor(new ));
        styleCenter.setAlignment(HorizontalAlignment.CENTER);
        styleCenter.setVerticalAlignment(VerticalAlignment.CENTER);
        styleRowHeader.setWrapText(true);
        styleCenter.setWrapText(true);
        styleRowHeader.setVerticalAlignment(VerticalAlignment.CENTER);
        styleRow.setWrapText(true);
        // border
        styleRow = setBorder(styleRow, BorderStyle.THIN);
        styleCenter = setBorder(styleCenter, BorderStyle.THIN);
        // list header
        List<String> fileHeaderList = new ArrayList<>();
        fileHeaderList.add("STT");
        fileHeaderList.add("Tên chiến dịch");
        fileHeaderList.add("Mã chiến dịch");
        fileHeaderList.add("Thời gian thực hiện");
        fileHeaderList.add("Số lượng khách hàng của chiến dịch");
        //for
        sheet = workbook.createSheet("sheet 1");
        // Header
        int startRowTable = 7;
        int count = 0;
        int colCurrent=0;
        // cot tinh
        Row rowtitle = sheet.createRow(startRowTable);
        Row rowHeader = sheet.createRow(startRowTable+1);
        Row rowHeader2 = sheet.createRow(startRowTable+2); // chứa số lượng, tỷ lệ
        for (int i = 0; i < fileHeaderList.size(); i++) {
            //merge tiêu đề tĩnh
            CellRangeAddress region = new CellRangeAddress(startRowTable, startRowTable+2, colCurrent, colCurrent);
            sheet.addMergedRegion(region);
            setBorderRegion(BorderStyle.THIN, region, sheet);
            WorkBookBuilder.writeCellContent(rowtitle, styleRowHeader, colCurrent, fileHeaderList.get(i));
            sheet.setColumnWidth(colCurrent, 4000);
            colCurrent++;
        }
        // cot trang thai contact status
        int colStatusContactStart = colCurrent;
        List<String> statusContactGroupByKey = new ArrayList<>(statusContactGroup.keySet());
        Collections.sort(statusContactGroupByKey); // sort asc
        if(statusContactGroup != null && statusContactGroup.size() >0){
            for (String key : statusContactGroupByKey) {
                WorkBookBuilder.writeCellContent(rowHeader, styleRowHeader, colCurrent, key);
                WorkBookBuilder.writeCellContent(rowHeader2, styleRowHeader, colCurrent, "Số lượng");
                WorkBookBuilder.writeCellContent(rowHeader2, styleRowHeader, colCurrent+1, "Tỷ lệ");
                sheet.setColumnWidth(colCurrent, 3000);
                sheet.setColumnWidth(colCurrent+1, 3000);
                // merge
                CellRangeAddress region = new CellRangeAddress(startRowTable+1, startRowTable+1, colCurrent, colCurrent+1);
                sheet.addMergedRegion(region);
                setBorderRegion(BorderStyle.THIN, region, sheet);
                colCurrent+=2;
            }
        }
        //merge tiêu đề contact status
        try{
            CellRangeAddress regionContact = new CellRangeAddress(startRowTable, startRowTable, colStatusContactStart, colCurrent-1);
            sheet.addMergedRegion(regionContact);
            setBorderRegion(BorderStyle.THIN, regionContact, sheet);
            WorkBookBuilder.writeCellContent(rowtitle, styleRowHeader, colStatusContactStart, "Trạng thái kết nối");
        } catch (Exception e){

        }


        // cot trang thai call status
        int colStatusCalltStart = colCurrent;
        List<String> statusCallGroupByKey = new ArrayList<>(statusCallGroup.keySet());
        Collections.sort(statusCallGroupByKey); // sort asc
        if(statusCallGroup != null && statusCallGroup.size() >0){
            for (String key : statusCallGroupByKey) {
                WorkBookBuilder.writeCellContent(rowHeader, styleRowHeader, colCurrent, key);
                WorkBookBuilder.writeCellContent(rowHeader2, styleRowHeader, colCurrent, "Số lượng");
                WorkBookBuilder.writeCellContent(rowHeader2, styleRowHeader, colCurrent+1, "Tỷ lệ");
                sheet.setColumnWidth(colCurrent, 3000);
                sheet.setColumnWidth(colCurrent+1, 3000);
                // merge
                CellRangeAddress region = new CellRangeAddress(startRowTable+1, startRowTable+1, colCurrent, colCurrent+1);
                sheet.addMergedRegion(region);
                setBorderRegion(BorderStyle.THIN, region, sheet);
                colCurrent+=2;
            }
        }
        //merge tiêu đề contact status
        try{
            CellRangeAddress region = new CellRangeAddress(startRowTable, startRowTable, colStatusCalltStart, colCurrent-1);
            sheet.addMergedRegion(region);
            setBorderRegion(BorderStyle.THIN, region, sheet);
            WorkBookBuilder.writeCellContent(rowtitle, styleRowHeader, colStatusCalltStart, "Trạng thái khảo sát");
        } catch (Exception e){

        }

        // độ hài lòng
        int colSatisfiedtStart = colCurrent;
        List<String> listSatisfied =new ArrayList<>();
        listSatisfied.add("Hài lòng"); listSatisfied.add("Bình thường"); listSatisfied.add("Không hài lòng");
        for (String key :  listSatisfied) {
            WorkBookBuilder.writeCellContent(rowHeader, styleRowHeader, colCurrent, key);
            WorkBookBuilder.writeCellContent(rowHeader2, styleRowHeader, colCurrent, "Số lượng");
            WorkBookBuilder.writeCellContent(rowHeader2, styleRowHeader, colCurrent+1, "Tỷ lệ");
            sheet.setColumnWidth(colCurrent, 3000);
            sheet.setColumnWidth(colCurrent+1, 3000);
            // merge
            CellRangeAddress regionStatisfied = new CellRangeAddress(rowHeader.getRowNum(), rowHeader.getRowNum(), colCurrent, colCurrent+1);
            sheet.addMergedRegion(regionStatisfied);
            setBorderRegion(BorderStyle.THIN, regionStatisfied, sheet);
            colCurrent+=2;
        }
        //merge tiêu đề độ hài lòng
        CellRangeAddress regionStatisfiedTitle = new CellRangeAddress(startRowTable, startRowTable, colSatisfiedtStart, colCurrent-1);
        sheet.addMergedRegion(regionStatisfiedTitle);
        setBorderRegion(BorderStyle.THIN, regionStatisfiedTitle, sheet);
        WorkBookBuilder.writeCellContent(rowtitle, styleRowHeader, colSatisfiedtStart, "Đo độ hài lòng khách hàng");

        // hiệu quả triển khai
        int colEfectDeploy = colCurrent;
        List<String> listEfectDeploy =new ArrayList<>();
        listEfectDeploy.add("Khách hàng thực hiện theo tư vấn"); listEfectDeploy.add("Khách hàng không thực hiện theo tư vấn");
        for (String key :  listEfectDeploy) {
            WorkBookBuilder.writeCellContent(rowHeader, styleRowHeader, colCurrent, key);
            WorkBookBuilder.writeCellContent(rowHeader2, styleRowHeader, colCurrent, "Số lượng");
            WorkBookBuilder.writeCellContent(rowHeader2, styleRowHeader, colCurrent+1, "Tỷ lệ");
            sheet.setColumnWidth(colCurrent, 3000);
            sheet.setColumnWidth(colCurrent+1, 3000);
            // merge
            CellRangeAddress regionEffect = new CellRangeAddress(rowHeader.getRowNum(), rowHeader.getRowNum(), colCurrent, colCurrent+1);
            sheet.addMergedRegion(regionEffect);
            setBorderRegion(BorderStyle.THIN, regionEffect, sheet);
            colCurrent+=2;
        }
        //merge tiêu đề Hiệu quả sau triển khai
        CellRangeAddress regionEffectTitle = new CellRangeAddress(startRowTable, startRowTable, colEfectDeploy, colCurrent-1);
        sheet.addMergedRegion(regionEffectTitle);
        setBorderRegion(BorderStyle.THIN, regionEffectTitle, sheet);
        WorkBookBuilder.writeCellContent(rowtitle, styleRowHeader, colEfectDeploy, "Hiệu quả sau triển khai");

        // fill data row
        count+=2;
        int order=0;
        NumberFormat nf = NumberFormat.getPercentInstance();
        nf.setMaximumFractionDigits(2);
        HashMap<Long, Long> listAmountStatusContact = getAmountStatus(listStatusContact);
        HashMap<Long, Long> listAmountStatusCall = getAmountStatus(listStatusCall);
        for (ReportEffectDTO row:listEffect) {// vòng lặp các chiến dịch
            int col = 0;
            count += 1;
            order += 1;
            Row rowI = sheet.createRow(startRowTable + count);
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, order);//stt
            WorkBookBuilder.writeCellContent(rowI, styleRow, col++, row.getCampaignName());//ten chiend dich
            WorkBookBuilder.writeCellContent(rowI, styleRow, col++, row.getCampaignCode());//ma
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getStartTime()+" - "+row.getEndTime());//thoi gian bd - kt
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getCustomerCount());// đếm
            // cột trạng thái kết nối
            for (String key : statusContactGroupByKey) {
                List<ReportEffectStatusDTO> lst = statusContactGroup.get(key);
                if(lst == null) lst = new ArrayList<>();
                // tìm xem có campaign_id nào trong trạng thái này ko
                boolean existStatus = false;// có tồn tại trạng thái trong campaign hiện tại hay ko
                for (ReportEffectStatusDTO rowStatus : lst) {
                    if(row.getCampaignId().equals(rowStatus.getCampaignId())){
                        float percentCount=0f;
                        if(listAmountStatusContact.get(row.getCampaignId()) >0){
                            percentCount= (float)rowStatus.getContactStatusCount()/listAmountStatusContact.get(row.getCampaignId());
                        }
                        WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, rowStatus.getContactStatusCount());
                        WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, nf.format(percentCount));
                        existStatus = true;
                        break;
                    }
                }
                if(!existStatus){
                    WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, 0);
                    WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, "0%");
                }
            }

            // cột trạng thái khảo sát
            for (String key : statusCallGroupByKey) {
                List<ReportEffectStatusDTO> lst = statusCallGroup.get(key);
                if(lst == null) lst = new ArrayList<>();
                // tìm xem có campaign_id nào trong trạng thái này ko
                boolean existStatus = false;// có tồn tại trạng thái trong campaign hiện tại hay ko
                for (ReportEffectStatusDTO rowStatus : lst) {
                    if(row.getCampaignId().equals(rowStatus.getCampaignId())){
                        float percentCount=0f;
                        if(listAmountStatusCall.get(row.getCampaignId()) >0){
                            percentCount= (float)rowStatus.getContactStatusCount()/listAmountStatusCall.get(row.getCampaignId());
                        }
                        WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, rowStatus.getContactStatusCount());
                        WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, nf.format(percentCount));
                        existStatus = true;
                        break;
                    }
                }
                if(!existStatus){
                    WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, 0);
                    WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, "0%");
                }
            }
            //độ hài lòng khách hàng
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getStatisfied());
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, nf.format(row.getStatisfied()/row.sumStatisfied()));
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getNormal());
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, nf.format(row.getNormal()/row.sumStatisfied()));
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getUnStatisfied());
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, nf.format(row.getUnStatisfied()/row.sumStatisfied()));
            // hiệu quả sau triển khai
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getFollow());
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, nf.format(row.getFollow()/row.sumFollow()));
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, row.getUnFollow());
            WorkBookBuilder.writeCellContent(rowI, styleCenter, col++, nf.format(row.getUnFollow()/row.sumFollow()));
        }
        // tiêu đề báo cáo
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, colCurrent-1));
        String titleReport = "Báo cáo";
        if(hmapParams.get("title_report") != null){
            titleReport = hmapParams.get("title_report").toString();
        }
        WorkBookBuilder.writeCellContent(sheet.createRow(1), styleRowHeader, 0, titleReport);
        // các tiêu chí
        Row properties1 = sheet.createRow(2);
        WorkBookBuilder.writeCellContent(properties1, styleProperties, 0, "Thời điểm xuất báo cáo");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        WorkBookBuilder.writeCellContent(properties1, styleProperties, 2, sdf.format(getCurrentDateByTimezone(hmapParams)));

        Row properties2 = sheet.createRow(3);
        WorkBookBuilder.writeCellContent(properties2, styleProperties, 0, "Mã chiến dịch");
        WorkBookBuilder.writeCellContent(properties2, styleProperties, 1, hmapParams.get("campaignCode").toString());

        Row properties3 = sheet.createRow(4);
        WorkBookBuilder.writeCellContent(properties3, styleProperties, 0, "Báo cáo từ ngày");
        if(hmapParams.get("createTimeFrom") != null){
            WorkBookBuilder.writeCellContent(properties3, styleProperties, 1, formatDateSearch(hmapParams.get("createTimeFrom").toString()));
        }
        if(hmapParams.get("createTimeTo") != null){
            WorkBookBuilder.writeCellContent(properties3, styleProperties, 2, "Đến ngày");
            WorkBookBuilder.writeCellContent(properties3, styleProperties, 3, formatDateSearch(hmapParams.get("createTimeTo").toString()));
        }

        Row properties4 = sheet.createRow(5);
        WorkBookBuilder.writeCellContent(properties4, styleProperties, 0, "Tài khoản");
        WorkBookBuilder.writeCellContent(properties4, styleProperties, 1, userSession.getUserName());
        return workbook;
    }
    public XSSFWorkbook exportReport(String reportName, HashMap<String, Object> hmapParams){
        if("general_by_campaign_report".equalsIgnoreCase(reportName)){
            return exportReportGeneral(hmapParams);
        }
        if("Report_Deatail_By_Campaign".equalsIgnoreCase(reportName)){
            return exportReportDeatail(hmapParams);
        }
        if("Report_Effect_By_Campaign".equalsIgnoreCase(reportName)){
            return exportReportEffect(hmapParams);
        }
        return new XSSFWorkbook();
    }
    // convert data
    private HashMap<String, GroupQuestion> convertGroupQuestion(List<ReportQuestionAnswerDTO> list ){
        //group theo cau hoi
        HashMap<String, GroupQuestion> hashMap = new HashMap<String, GroupQuestion>();
        if(list == null) return hashMap;
        for (ReportQuestionAnswerDTO row : list) {
            if(!hashMap.containsKey(row.getQuestion())){// nếu chưa có câu hỏi
                GroupQuestion groupQuestion = new GroupQuestion();
                groupQuestion.listAnswer.add(row);
                groupQuestion.totalCount += row.getAnswerCount();
                hashMap.put(row.getQuestion(), groupQuestion);
            } else {
                hashMap.get(row.getQuestion()).listAnswer.add(row);
                hashMap.get(row.getQuestion()).totalCount += row.getAnswerCount();
            }
        }
        return hashMap;
    }
    // convert data
    private HashMap<String, Long> convertGroupQuestionOrderIndex(List<ReportQuestionAnswerDTO> list ){
        //group theo cau hoi
        HashMap<String, Long> hashMap = new HashMap<String, Long>();
        if(list == null) return hashMap;
        for (ReportQuestionAnswerDTO row : list) {
            if(!hashMap.containsKey(row.getQuestion())){// nếu chưa có câu hỏi
                hashMap.put(row.getQuestion(), row.getQuestionOrderIndex());
            } else {
                //hashMap.get(row.getQuestion()).listAnswer.add(row);
            }
        }
        return hashMap;
    }
    private HashMap<String, List<ReportEffectStatusDTO>> convertGroupStatus(List<ReportEffectStatusDTO> list ){
        //group theo tranjg thai
        HashMap<String, List<ReportEffectStatusDTO>> hashMap = new HashMap<String, List<ReportEffectStatusDTO>>();
        if(list == null) return hashMap;
        for (ReportEffectStatusDTO row : list) {
            if(!hashMap.containsKey(row.getCompleteName())){// nếu chưa có chiến dịch
                List<ReportEffectStatusDTO> lst = new ArrayList<>();
                lst.add(row);
                hashMap.put(row.getCompleteName(), lst);
            } else {
                hashMap.get(row.getCompleteName()).add(row);
            }
        }
        return hashMap;
    }
    private HashMap<String, List<ReportCusInfoDTO>> convertCusInfoGroupByTitle(List<ReportCusInfoDTO> list ){
        //group theo title
        HashMap<String, List<ReportCusInfoDTO>> hashMap = new HashMap<String, List<ReportCusInfoDTO>>();
        if(list == null) return hashMap;
        for (ReportCusInfoDTO row : list) {
            if(!hashMap.containsKey(row.getTitle())){
                List<ReportCusInfoDTO> lst = new ArrayList<>();
                lst.add(row);
                hashMap.put(row.getTitle(), lst);
            } else {
                hashMap.get(row.getTitle()).add(row);
            }
        }
        return hashMap;
    }
    private HashMap<Long, Long> getAmountStatus(List<ReportEffectStatusDTO> list ){
        //group theo chiến dịch để tính tổng số trnajg thái
        HashMap<Long, Long> hashMap = new HashMap<Long, Long>();
        if(list == null) return hashMap;
        for (ReportEffectStatusDTO row : list) {
            if(!hashMap.containsKey(row.getCampaignId())){// nếu chưa có chiến dịch
                Long amount=row.getContactStatusCount();
                hashMap.put(row.getCampaignId(), amount);
            } else {
                Long amount = hashMap.get(row.getCampaignId());
                hashMap.replace(row.getCampaignId(), amount+row.getContactStatusCount());
            }
        }
        return hashMap;
    }
    public int getCountData(String reportName, HashMap<String, Object> hmapParams){
        // bo param pagesze
        if(hmapParams.containsKey("page"))
            hmapParams.put("page", 0);
        if(hmapParams.containsKey("pageSize"))
            hmapParams.put("pageSize", 99999999);
        if("general_by_campaign_report".equalsIgnoreCase(reportName)){
            return getStatistic(hmapParams).size();
        }
        if("Report_Deatail_By_Campaign".equalsIgnoreCase(reportName)){
            return getDetail(hmapParams).size();
        }
        if("Report_Effect_By_Campaign".equalsIgnoreCase(reportName)){
            return getEffect(hmapParams).size();
        }
        return 10;// pageSize default
    }
    // get data from db
    public List<ReportQuestionAnswerDTO> getQuestionAnswer2(HashMap<String, Object> hmapParams) {
        try {
            String runSP = "{ call IPCC_CCMS.QUESTION_ANSWER2(?,?,?,?,?) }";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_YYYY);

            Connection conn = getCon();
            CallableStatement cs = conn.prepareCall(runSP);
            cs.registerOutParameter(1,OracleTypes.CURSOR);
            if(hmapParams.get("campaignCode") != null){
                cs.setString(2,hmapParams.get("campaignCode").toString());
            }else{
                cs.setString(2,null);
            }
            if(hmapParams.get("companySiteId") != null){
                cs.setLong(3,Long.parseLong(hmapParams.get("companySiteId").toString()));
            }else{
                cs.setLong(3,0l);
            }
            if(hmapParams.get("timezone") != null){
                cs.setLong(4,Long.parseLong(hmapParams.get("timezone").toString()));
            }else{
                cs.setLong(4,0l);
            }
            if(hmapParams.get("timezoneOffset") != null){
                cs.setLong(5,Long.parseLong(hmapParams.get("timezoneOffset").toString()));
            }else{
                cs.setLong(5,0l);
            }
            cs.execute();
            ResultSet  rs  = (ResultSet) cs.getObject(1);
            List<ReportQuestionAnswerDTO> lstQuestionAnswer;
            lstQuestionAnswer = new ArrayList();
            while (rs.next()) {
                ReportQuestionAnswerDTO row;
                Long campaignId = rs.getLong(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.CAMPAIGN_ID);
                String campaignCode = rs.getString(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.CAMPAIGN_CODE);
                Long questionId = rs.getLong(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.SCENARIO_QUESTION_ID);
                String question = rs.getString(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.QUESTION);
                Long answerId = rs.getLong(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.SCENARIO_ANSWER_ID);
                String answer = rs.getString(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.ANSWER);
                Long answerCount = rs.getLong(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.ANSWER_COUNT);
                Long questionOrderIndex = rs.getLong(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.QUESTION_ORDER_INDEX);
                Long answerOrderIndex = rs.getLong(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.ANSWER_ORDER_INDEX);
                row = new ReportQuestionAnswerDTO(campaignId,campaignCode,questionId,question,answerId,answer,answerCount,questionOrderIndex,answerOrderIndex);

                lstQuestionAnswer.add(row);
            }
            return lstQuestionAnswer;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<ReportQuestionAnswerDTO> getStatistic(HashMap<String, Object> hmapParams) {
        try {
            String runSP = "{ call IPCC_CCMS.REPORT_STATISTIC(?,?,?,?,?,?,?) }";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_YYYY);
            Connection conn = getCon();
            CallableStatement cs = conn.prepareCall(runSP);
            cs.registerOutParameter(1,OracleTypes.CURSOR);
            if(hmapParams.get("campaignCode") != null){
                cs.setString(2,hmapParams.get("campaignCode").toString());
            }else{
                cs.setString(2,null);
            }
            if(hmapParams.get("companySiteId") != null){
                cs.setLong(3,Long.parseLong(hmapParams.get("companySiteId").toString()));
            }else{
                cs.setLong(3,0l);
            }
            // ngay
            if(hmapParams.get("createTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("createTimeFrom").toString(), formatter);
                cs.setDate(4, createTimeFrom == null ? null : Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(4, null);
            }
            if(hmapParams.get("createTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("createTimeTo").toString(), formatter);
                cs.setDate(5, createTimeTo == null ? null : Date.valueOf(createTimeTo));
            } else{
                cs.setDate(5, null);
            }
            if(hmapParams.get("timezone") != null){
                cs.setLong(6,Long.parseLong(hmapParams.get("timezone").toString()));
            }else{
                cs.setLong(6,0l);
            }
            if(hmapParams.get("timezoneOffset") != null){
                cs.setLong(7,Long.parseLong(hmapParams.get("timezoneOffset").toString()));
            }else{
                cs.setLong(7,0l);
            }
            cs.execute();
            ResultSet  rs  = (ResultSet) cs.getObject(1);
            List<ReportQuestionAnswerDTO> lstQuestionAnswer;
            lstQuestionAnswer = new ArrayList();
            while (rs.next()) {
                ReportQuestionAnswerDTO row;
                Long campaignId = rs.getLong(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.CAMPAIGN_ID);
                String campaignCode = rs.getString(ReportColDTO.PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN.CAMPAIGN_CODE);
                String question = ReportColDTO.PROC_GET_STATISTIC_BY_CAMPAIGNCODE.QUESTION;
                String answer = rs.getString(ReportColDTO.PROC_GET_STATISTIC_BY_CAMPAIGNCODE.COMPLETE_NAME);
                Long answerCount = rs.getLong(ReportColDTO.PROC_GET_STATISTIC_BY_CAMPAIGNCODE.AMOUNT);
                row = new ReportQuestionAnswerDTO(campaignId,campaignCode,null,question,null,answer,answerCount);
                lstQuestionAnswer.add(row);
            }
            return lstQuestionAnswer;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<ReportDetailDTO> getDetail(HashMap<String, Object> hmapParams) {
        try {
            String runSP = "{ call IPCC_CCMS.REPORT_DETAIL(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_YYYY);
            Connection conn = getCon();
            CallableStatement cs = conn.prepareCall(runSP);
            cs.registerOutParameter(1,OracleTypes.CURSOR);
            if(hmapParams.get("keyword") != null){
                cs.setString(2,hmapParams.get("keyword").toString());
            }else{
                cs.setString(2,null);
            }
            if(hmapParams.get("campaignCode") != null){
                cs.setString(3,hmapParams.get("campaignCode").toString());
            }else{
                cs.setString(3,null);
            }
            if(hmapParams.get("startTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("startTimeFrom").toString(), formatter);
                cs.setDate(4, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(4, null);
            }
            if(hmapParams.get("startTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("startTimeTo").toString(), formatter);
                cs.setDate(5, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(5, null);
            }
            if(hmapParams.get("endTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("endTimeFrom").toString(), formatter);
                cs.setDate(6, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(6, null);
            }
            if(hmapParams.get("endTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("endTimeTo").toString(), formatter);
                cs.setDate(7, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(7, null);
            }
            if(hmapParams.get("companySiteId") != null){
                cs.setLong(8,Long.parseLong(hmapParams.get("companySiteId").toString()));
            }else{
                cs.setLong(8,0l);
            }
            if(hmapParams.get("page") != null){
                cs.setLong(9,Long.parseLong(hmapParams.get("page").toString()));
            }else{
                cs.setLong(9,ReportColDTO.PAGE);
            }
            if(hmapParams.get("pageSize") != null){
                cs.setLong(10,Long.parseLong(hmapParams.get("pageSize").toString()));
            }else{
                cs.setLong(10,ReportColDTO.PAGE_SIZE);
            }
            if(hmapParams.get("createTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("createTimeFrom").toString(), formatter);
                cs.setDate(11, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(11, null);
            }
            if(hmapParams.get("createTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("createTimeTo").toString(), formatter);
                cs.setDate(12, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(12, null);
            }
            if(hmapParams.get("fullName") != null){
                cs.setString(13,hmapParams.get("fullName").toString());
            }else{
                cs.setString(13,null);
            }
            if(hmapParams.get("timezone") != null){
                cs.setLong(14,Long.parseLong(hmapParams.get("timezone").toString()));
            }else{
                cs.setLong(14,0l);
            }
            if(hmapParams.get("timezoneOffset") != null){
                cs.setLong(15,Long.parseLong(hmapParams.get("timezoneOffset").toString()));
            }else{
                cs.setLong(15,0l);
            }
            cs.execute();
            ResultSet  rs  = (ResultSet) cs.getObject(1);
            List<ReportDetailDTO> lstReportDetailDTO;
            lstReportDetailDTO = new ArrayList();
            while (rs.next()) {
                ReportDetailDTO row;
                Long contactCustResultId = rs.getLong(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.CONTACT_CUST_RESULT_ID);
                Long campaignId = rs.getLong(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.CAMPAIGN_ID);
                String campaignName = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.CAMPAIGN_NAME);
                String campaignCode = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.CAMPAIGN_CODE);
                String startTime = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.START_TIME);
                String endTime = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.END_TIME);
                String phoneNumber = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.PHONE_NUMBER);
                String name = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.NAME);
                Long customerId = rs.getLong(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.CUSTOMER_ID);
                String startCall = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.START_CALL);
                String duarationCall = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.DURATION_CALL);
                String userName = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.USER_NAME);
                String completeName = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.COMPLETE_NAME);
                String description = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.DESCRIPTION);
                row = new ReportDetailDTO(contactCustResultId,campaignId,campaignName,campaignCode,startTime,endTime,phoneNumber,name,customerId,startCall,duarationCall,userName,completeName,description);
                lstReportDetailDTO.add(row);
            }
            return lstReportDetailDTO;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<ReportCusInfoDTO> getCusInfo(HashMap<String, Object> hmapParams) {
        try {
            String runSP = "{ call IPCC_CCMS.CUS_INFO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_YYYY);
            Connection conn = getCon();
            CallableStatement cs = conn.prepareCall(runSP);
            cs.registerOutParameter(1,OracleTypes.CURSOR);
            if(hmapParams.get("keyword") != null){
                cs.setString(2,hmapParams.get("keyword").toString());
            }else{
                cs.setString(2,null);
            }
            if(hmapParams.get("campaignCode") != null){
                cs.setString(3,hmapParams.get("campaignCode").toString());
            }else{
                cs.setString(3,null);
            }
            if(hmapParams.get("startTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("startTimeFrom").toString(), formatter);
                cs.setDate(4, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(4, null);
            }
            if(hmapParams.get("startTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("startTimeTo").toString(), formatter);
                cs.setDate(5, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(5, null);
            }
            if(hmapParams.get("endTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("endTimeFrom").toString(), formatter);
                cs.setDate(6, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(6, null);
            }
            if(hmapParams.get("endTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("endTimeTo").toString(), formatter);
                cs.setDate(7, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(7, null);
            }
            if(hmapParams.get("companySiteId") != null){
                cs.setLong(8,Long.parseLong(hmapParams.get("companySiteId").toString()));
            }else{
                cs.setLong(8,0l);
            }
            if(hmapParams.get("page") != null){
                cs.setLong(9,Long.parseLong(hmapParams.get("page").toString()));
            }else{
                cs.setLong(9,ReportColDTO.PAGE);
            }
            if(hmapParams.get("pageSize") != null){
                cs.setLong(10,Long.parseLong(hmapParams.get("pageSize").toString()));
            }else{
                cs.setLong(10,ReportColDTO.PAGE_SIZE);
            }
            if(hmapParams.get("createTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("createTimeFrom").toString(), formatter);
                cs.setDate(11, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(11, null);
            }
            if(hmapParams.get("createTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("createTimeTo").toString(), formatter);
                cs.setDate(12, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(12, null);
            }
            if(hmapParams.get("fullName") != null){
                cs.setString(13,hmapParams.get("fullName").toString());
            }else{
                cs.setString(13,null);
            }
            if(hmapParams.get("timezone") != null){
                cs.setLong(14,Long.parseLong(hmapParams.get("timezone").toString()));
            }else{
                cs.setLong(14,0l);
            }
            if(hmapParams.get("timezoneOffset") != null){
                cs.setLong(15,Long.parseLong(hmapParams.get("timezoneOffset").toString()));
            }else{
                cs.setLong(15,0l);
            }
            cs.execute();
            ResultSet  rs  = (ResultSet) cs.getObject(1);
            List<ReportCusInfoDTO> lstReportCusInfoDTO;
            lstReportCusInfoDTO = new ArrayList();
            while (rs.next()) {
                ReportCusInfoDTO row;
                Long contactCustResultId = rs.getLong(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.CONTACT_CUST_RESULT_ID);
                String title = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.TITLE);
                String value = rs.getString(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.VALUE);
                Long customerId = rs.getLong(ReportColDTO.PROC_GET_REPORT_DETAIL_BY_CAMPAIGN.CUSTOMER_ID);
                row = new ReportCusInfoDTO(customerId,title,value,contactCustResultId);
                lstReportCusInfoDTO.add(row);
            }
            return lstReportCusInfoDTO;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<ReportEffectDTO> getEffect(HashMap<String, Object> hmapParams) {
        try {
            String runSP = "{ call IPCC_CCMS.REPORT_EFFECT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_YYYY);
            Connection conn = getCon();
            CallableStatement cs = conn.prepareCall(runSP);
            cs.registerOutParameter(1,OracleTypes.CURSOR);
            if(hmapParams.get("keyword") != null){
                cs.setString(2,hmapParams.get("keyword").toString());
            }else{
                cs.setString(2,null);
            }
            if(hmapParams.get("campaignCode") != null){
                cs.setString(3,hmapParams.get("campaignCode").toString());
            }else{
                cs.setString(3,null);
            }
            if(hmapParams.get("campaignName") != null){
                cs.setString(4,hmapParams.get("campaignName").toString());
            }else{
                cs.setString(4,null);
            }
            if(hmapParams.get("startTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("startTimeFrom").toString(), formatter);
                cs.setDate(5, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(5, null);
            }
            if(hmapParams.get("startTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("startTimeTo").toString(), formatter);
                cs.setDate(6, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(6, null);
            }
            if(hmapParams.get("endTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("endTimeFrom").toString(), formatter);
                cs.setDate(7, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(7, null);
            }
            if(hmapParams.get("endTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("endTimeTo").toString(), formatter);
                cs.setDate(8, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(8, null);
            }
            if(hmapParams.get("companySiteId") != null){
                cs.setLong(9,Long.parseLong(hmapParams.get("companySiteId").toString()));
            }else{
                cs.setLong(9,0l);
            }
            if(hmapParams.get("page") != null){
                cs.setLong(10,Long.parseLong(hmapParams.get("page").toString()));
            }else{
                cs.setLong(10,ReportColDTO.PAGE);
            }
            if(hmapParams.get("pageSize") != null){
                cs.setLong(11,Long.parseLong(hmapParams.get("pageSize").toString()));
            }else{
                cs.setLong(11,ReportColDTO.PAGE_SIZE);
            }
            if(hmapParams.get("createTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("createTimeFrom").toString(), formatter);
                cs.setDate(12, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(12, null);
            }
            if(hmapParams.get("createTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("createTimeTo").toString(), formatter);
                cs.setDate(13, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(13, null);
            }
            if(hmapParams.get("timezone") != null){
                cs.setLong(14,Long.parseLong(hmapParams.get("timezone").toString()));
            }else{
                cs.setLong(14,0l);
            }
            if(hmapParams.get("timezoneOffset") != null){
                cs.setLong(15,Long.parseLong(hmapParams.get("timezoneOffset").toString()));
            }else{
                cs.setLong(15,0l);
            }
            cs.execute();
            ResultSet  rs  = (ResultSet) cs.getObject(1);
            List<ReportEffectDTO> lstReportEffectDTO;
            lstReportEffectDTO = new ArrayList();
            while (rs.next()) {
                ReportEffectDTO row;
                Long campaignId = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.CAMPAIGN_ID);
                String campaignName = rs.getString(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.CAMPAIGN_NAME);
                String campaignCode = rs.getString(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.CAMPAIGN_CODE);
                String startTime = rs.getString(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.START_TIME);
                String endTime = rs.getString(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.END_TIME);
                Long customerCount = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.CUSTOMER_COUNT);
                Long statisfied = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.SATISFIED);
                Long normal = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.NORMAL);
                Long unStatisfied = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.UNSATISFIED);
                Long follow = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.FOLLOW);
                Long unFollow = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.UNFOLLOW);

                row = new ReportEffectDTO(campaignId,campaignCode,campaignName,startTime,endTime,customerCount,statisfied,normal,unStatisfied,follow,unFollow);
                lstReportEffectDTO.add(row);
            }
            return lstReportEffectDTO;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<ReportEffectStatusDTO> getEffectStatus(HashMap<String, Object> hmapParams) {
        try {
            String runSP = "{ call IPCC_CCMS.REPORT_EFFECT_STATUS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_YYYY);
            Connection conn = getCon();
            CallableStatement cs = conn.prepareCall(runSP);
            cs.registerOutParameter(1,OracleTypes.CURSOR);
            if(hmapParams.get("keyword") != null){
                cs.setString(2,hmapParams.get("keyword").toString());
            }else{
                cs.setString(2,null);
            }
            if(hmapParams.get("campaignCode") != null){
                cs.setString(3,hmapParams.get("campaignCode").toString());
            }else{
                cs.setString(3,null);
            }
            if(hmapParams.get("campaignName") != null){
                cs.setString(4,hmapParams.get("campaignName").toString());
            }else{
                cs.setString(4,null);
            }
            if(hmapParams.get("startTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("startTimeFrom").toString(), formatter);
                cs.setDate(5, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(5, null);
            }
            if(hmapParams.get("startTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("startTimeTo").toString(), formatter);
                cs.setDate(6, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(6, null);
            }
            if(hmapParams.get("endTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("endTimeFrom").toString(), formatter);
                cs.setDate(7, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(7, null);
            }
            if(hmapParams.get("endTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("endTimeTo").toString(), formatter);
                cs.setDate(8, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(8, null);
            }
            if(hmapParams.get("companySiteId") != null){
                cs.setLong(9,Long.parseLong(hmapParams.get("companySiteId").toString()));
            }else{
                cs.setLong(9,0l);
            }
            if(hmapParams.get("page") != null){
                cs.setLong(10,Long.parseLong(hmapParams.get("page").toString()));
            }else{
                cs.setLong(10,ReportColDTO.PAGE);
            }
            if(hmapParams.get("pageSize") != null){
                cs.setLong(11,Long.parseLong(hmapParams.get("pageSize").toString()));
            }else{
                cs.setLong(11,ReportColDTO.PAGE_SIZE);
            }
            if(hmapParams.get("createTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("createTimeFrom").toString(), formatter);
                cs.setDate(12, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(12, null);
            }
            if(hmapParams.get("createTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("createTimeTo").toString(), formatter);
                cs.setDate(13, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(13, null);
            }
            if(hmapParams.get("timezone") != null){
                cs.setLong(14,Long.parseLong(hmapParams.get("timezone").toString()));
            }else{
                cs.setLong(14,0l);
            }
            if(hmapParams.get("timezoneOffset") != null){
                cs.setLong(15,Long.parseLong(hmapParams.get("timezoneOffset").toString()));
            }else{
                cs.setLong(15,0l);
            }
            cs.execute();
            ResultSet  rs  = (ResultSet) cs.getObject(1);
            List<ReportEffectStatusDTO> lstReportEffectStatusDTO;
            lstReportEffectStatusDTO = new ArrayList();
            while (rs.next()) {
                ReportEffectStatusDTO row;
                Long campaignId = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.CAMPAIGN_ID);
                Long contactStatus = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.CONTACT_STATUS);
                String completeName = rs.getString(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.COMPLETE_NAME);
                Long contactStatusCount = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.CONTACT_STATUS_COUNT);

                row = new ReportEffectStatusDTO(campaignId,contactStatus,completeName,contactStatusCount);
                lstReportEffectStatusDTO.add(row);
            }
            return lstReportEffectStatusDTO;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<ReportEffectStatusDTO> getEffectStatusCall(HashMap<String, Object> hmapParams) {
        try {
            String runSP = "{ call IPCC_CCMS.REPORT_EFFECT_CALL_STATUS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_YYYY);
            Connection conn = getCon();
            CallableStatement cs = conn.prepareCall(runSP);
            cs.registerOutParameter(1,OracleTypes.CURSOR);
            if(hmapParams.get("keyword") != null){
                cs.setString(2,hmapParams.get("keyword").toString());
            }else{
                cs.setString(2,null);
            }
            if(hmapParams.get("campaignCode") != null){
                cs.setString(3,hmapParams.get("campaignCode").toString());
            }else{
                cs.setString(3,null);
            }
            if(hmapParams.get("campaignName") != null){
                cs.setString(4,hmapParams.get("campaignName").toString());
            }else{
                cs.setString(4,null);
            }
            if(hmapParams.get("startTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("startTimeFrom").toString(), formatter);
                cs.setDate(5, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(5, null);
            }
            if(hmapParams.get("startTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("startTimeTo").toString(), formatter);
                cs.setDate(6, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(6, null);
            }
            if(hmapParams.get("endTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("endTimeFrom").toString(), formatter);
                cs.setDate(7, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(7, null);
            }
            if(hmapParams.get("endTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("endTimeTo").toString(), formatter);
                cs.setDate(8, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(8, null);
            }
            if(hmapParams.get("companySiteId") != null){
                cs.setLong(9,Long.parseLong(hmapParams.get("companySiteId").toString()));
            }else{
                cs.setLong(9,0l);
            }
            if(hmapParams.get("page") != null){
                cs.setLong(10,Long.parseLong(hmapParams.get("page").toString()));
            }else{
                cs.setLong(10,ReportColDTO.PAGE);
            }
            if(hmapParams.get("pageSize") != null){
                cs.setLong(11,Long.parseLong(hmapParams.get("pageSize").toString()));
            }else{
                cs.setLong(11,ReportColDTO.PAGE_SIZE);
            }
            if(hmapParams.get("createTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("createTimeFrom").toString(), formatter);
                cs.setDate(12, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(12, null);
            }
            if(hmapParams.get("createTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("createTimeTo").toString(), formatter);
                cs.setDate(13, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(13, null);
            }
            if(hmapParams.get("timezone") != null){
                cs.setLong(14,Long.parseLong(hmapParams.get("timezone").toString()));
            }else{
                cs.setLong(14,0l);
            }
            if(hmapParams.get("timezoneOffset") != null){
                cs.setLong(15,Long.parseLong(hmapParams.get("timezoneOffset").toString()));
            }else{
                cs.setLong(15,0l);
            }
            cs.execute();
            ResultSet  rs  = (ResultSet) cs.getObject(1);
            List<ReportEffectStatusDTO> lstReportEffectStatusDTO;
            lstReportEffectStatusDTO = new ArrayList();
            while (rs.next()) {
                ReportEffectStatusDTO row;
                Long campaignId = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.CAMPAIGN_ID);
                Long callStatus = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.CALL_STATUS);
                String completeName = rs.getString(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.COMPLETE_NAME);
                Long contactStatusCount = rs.getLong(ReportColDTO.PROC_GET_REPORT_EFECT_BY_CAMPAIGN.CALL_STATUS_COUNT);
                row = new ReportEffectStatusDTO(campaignId,callStatus,completeName,contactStatusCount);
                lstReportEffectStatusDTO.add(row);
            }
            return lstReportEffectStatusDTO;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<ReportQuestionAnswerDTO> getQuestionAnswer(HashMap<String, Object> hmapParams) {
        try {
            String runSP = "{ call IPCC_CCMS.QUESTION_ANSWER(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_YYYY);
            Connection conn = getCon();
            CallableStatement cs = conn.prepareCall(runSP);
            cs.registerOutParameter(1,OracleTypes.CURSOR);
            if(hmapParams.get("keyword") != null){
                cs.setString(2,hmapParams.get("keyword").toString());
            }else{
                cs.setString(2,null);
            }
            if(hmapParams.get("campaignCode") != null){
                cs.setString(3,hmapParams.get("campaignCode").toString());
            }else{
                cs.setString(3,null);
            }
            if(hmapParams.get("startTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("startTimeFrom").toString(), formatter);
                cs.setDate(4, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(4, null);
            }
            if(hmapParams.get("startTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("startTimeTo").toString(), formatter);
                cs.setDate(5, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(5, null);
            }
            if(hmapParams.get("endTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("endTimeFrom").toString(), formatter);
                cs.setDate(6, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(6, null);
            }
            if(hmapParams.get("endTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("endTimeTo").toString(), formatter);
                cs.setDate(7, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(7, null);
            }
            if(hmapParams.get("companySiteId") != null){
                cs.setLong(8,Long.parseLong(hmapParams.get("companySiteId").toString()));
            }else{
                cs.setLong(8,0l);
            }
            if(hmapParams.get("page") != null){
                cs.setLong(9,Long.parseLong(hmapParams.get("page").toString()));
            }else{
                cs.setLong(9,ReportColDTO.PAGE);
            }
            if(hmapParams.get("pageSize") != null){
                cs.setLong(10,Long.parseLong(hmapParams.get("pageSize").toString()));
            }else{
                cs.setLong(10,ReportColDTO.PAGE_SIZE);
            }
            if(hmapParams.get("createTimeFrom") != null){
                LocalDate createTimeFrom = LocalDate.parse(hmapParams.get("createTimeFrom").toString(), formatter);
                cs.setDate(11, Date.valueOf(createTimeFrom));
            } else{
                cs.setDate(11, null);
            }
            if(hmapParams.get("createTimeTo") != null){
                LocalDate createTimeTo = LocalDate.parse(hmapParams.get("createTimeTo").toString(), formatter);
                cs.setDate(12, Date.valueOf(createTimeTo));
            } else{
                cs.setDate(12, null);
            }
            if(hmapParams.get("fullName") != null){
                cs.setString(13,hmapParams.get("fullName").toString());
            }else{
                cs.setString(13,null);
            }
            if(hmapParams.get("timezone") != null){
                cs.setLong(14,Long.parseLong(hmapParams.get("timezone").toString()));
            }else{
                cs.setLong(14,0l);
            }
            if(hmapParams.get("timezoneOffset") != null){
                cs.setLong(15,Long.parseLong(hmapParams.get("timezoneOffset").toString()));
            }else{
                cs.setLong(15,0l);
            }
            cs.execute();
            ResultSet  rs  = (ResultSet) cs.getObject(1);
            List<ReportQuestionAnswerDTO> lstQuestionAnswer;
            lstQuestionAnswer = new ArrayList();
            while (rs.next()) {
                ReportQuestionAnswerDTO row;
                Long campaignId = rs.getLong(ReportColDTO.QUESTION_ANSWER.CAMPAIGN_ID);
                String campaignCode = rs.getString(ReportColDTO.QUESTION_ANSWER.CAMPAIGN_CODE);
                Long questionId = rs.getLong(ReportColDTO.QUESTION_ANSWER.SCENARIO_QUESTION_ID);
                String question = rs.getString(ReportColDTO.QUESTION_ANSWER.QUESTION);
                Long answerId = rs.getLong(ReportColDTO.QUESTION_ANSWER.SCENARIO_ANSWER_ID);
                String answer = rs.getString(ReportColDTO.QUESTION_ANSWER.ANSWER);
                Long contactCusResultId = rs.getLong(ReportColDTO.QUESTION_ANSWER.CONTACT_CUST_RESULT_ID);
                Long questionOrderIndex = rs.getLong(ReportColDTO.QUESTION_ANSWER.QUESTION_ORDER_INDEX);
                Long answerOrderIndex = rs.getLong(ReportColDTO.QUESTION_ANSWER.ANSWER_ORDER_INDEX);
                row = new ReportQuestionAnswerDTO(campaignId,campaignCode,questionId,question,answerId,answer,0l,questionOrderIndex,answerOrderIndex);
                row.setContactCusResultId(contactCusResultId);
                lstQuestionAnswer.add(row);
            }
            return lstQuestionAnswer;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public String getByCampaignNameByCode(String campaignCode){
        Connection conn = getCon();
        String campaignName = "";
        try {
            String query = "select campaign_name from campaign where campaign_code = '" + campaignCode + "' and rownum = 1";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next()){
                campaignName = rs.getString("campaign_name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return campaignName;
    }
    // connect
    public Connection getCon(){
        entityManager = entityManager.getEntityManagerFactory().createEntityManager();
        Session hibernateSession = entityManager.unwrap(Session.class);
        Connection conn = ((SessionImpl) hibernateSession).connection();
//        try {
//            String url = "";
//            conn = DriverManager.getConnection(
//                    "jdbc:oracle:thin:@14.225.5.244:1521:orcl", "IPCC_CCMS", "IPCC_CCMS#123");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        return conn;
    }

    public UserSession getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    private java.util.Date getCurrentDateByTimezone(HashMap<String, Object> hmapParams){
        int timezoneOffset = 0;
        if(hmapParams.get("timezoneOffset") != null){
            timezoneOffset = Integer.parseInt(hmapParams.get("timezoneOffset").toString());
        }
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, (int) (timezoneOffset/60));
        return cal.getTime();
    }

    private String formatDateSearch(String date){
        if(date == null || date.trim().length() < 1) return date;
        String[] strList = date.split("-");
        if(strList.length <3) return date;
        String result = strList[2] + "/" + strList[1] + "/" + strList[0];
        return result;
    }
}
