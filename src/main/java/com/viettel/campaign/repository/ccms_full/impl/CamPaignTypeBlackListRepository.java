package com.viettel.campaign.repository.ccms_full.impl;

import com.viettel.campaign.model.ccms_full.CampaignTypeBlackList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CamPaignTypeBlackListRepository extends JpaRepository<CampaignTypeBlackList, Long> {

    @Query(value = "SELECT * FROM CAMPAIGN_BLACKLIST_CONFIG WHERE CAMPAIGN_TYPE_ID = ?1 and STATUS = 1", nativeQuery = true)
    CampaignTypeBlackList findAllByCampaignTypeId(Long campaignTypeId);

    @Query(value = "SELECT * FROM CAMPAIGN_BLACKLIST_CONFIG WHERE CAMPAIGN_BLACKLIST_CONFIG_ID = ?1 and STATUS = 1", nativeQuery = true)
    CampaignTypeBlackList findByCampaignBlacklistConfigId(Long campaignBlacklistConfigId);

//    @Query(value = "UPDATE CAMPAIGN_BLACKLIST_CONFIG SET CAMPAIGN_TYPE_ID = ?3 , CAMPAIGN_BLACKLIST_CAT_ID = ?2 WHERE CAMPAIGN_BLACKLIST_CONFIG_ID = ?1", nativeQuery = true)
//    void updateCampaignBlackListConfig(Long campaignBlacklistConfigId,Long campaignBlacklistCatId, Long campaignTypeId);

}
