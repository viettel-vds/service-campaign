package com.viettel.campaign.model.ccms_full;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Time;
import java.util.Objects;

@Entity
public class Calendar {
    private int calendarId;
    private String calendarName;
    private String description;
    private Time createDate;
    private String createBy;
    private Time updateDate;
    private String updateBy;
    private Boolean status;
    private Integer siteId;
    private Integer calendarType;
    private Integer sourceId;
    private Integer channelId;

    @Id
    @Column(name = "CALENDAR_ID")
    public int getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(int calendarId) {
        this.calendarId = calendarId;
    }

    @Basic
    @Column(name = "CALENDAR_NAME")
    public String getCalendarName() {
        return calendarName;
    }

    public void setCalendarName(String calendarName) {
        this.calendarName = calendarName;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "CREATE_DATE")
    public Time getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Time createDate) {
        this.createDate = createDate;
    }

    @Basic
    @Column(name = "CREATE_BY")
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Basic
    @Column(name = "UPDATE_DATE")
    public Time getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Time updateDate) {
        this.updateDate = updateDate;
    }

    @Basic
    @Column(name = "UPDATE_BY")
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Basic
    @Column(name = "STATUS")
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "CALENDAR_TYPE")
    public Integer getCalendarType() {
        return calendarType;
    }

    public void setCalendarType(Integer calendarType) {
        this.calendarType = calendarType;
    }

    @Basic
    @Column(name = "SOURCE_ID")
    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    @Basic
    @Column(name = "CHANNEL_ID")
    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Calendar calendar = (Calendar) o;
        return calendarId == calendar.calendarId &&
                Objects.equals(calendarName, calendar.calendarName) &&
                Objects.equals(description, calendar.description) &&
                Objects.equals(createDate, calendar.createDate) &&
                Objects.equals(createBy, calendar.createBy) &&
                Objects.equals(updateDate, calendar.updateDate) &&
                Objects.equals(updateBy, calendar.updateBy) &&
                Objects.equals(status, calendar.status) &&
                Objects.equals(siteId, calendar.siteId) &&
                Objects.equals(calendarType, calendar.calendarType) &&
                Objects.equals(sourceId, calendar.sourceId) &&
                Objects.equals(channelId, calendar.channelId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(calendarId, calendarName, description, createDate, createBy, updateDate, updateBy, status, siteId, calendarType, sourceId, channelId);
    }
}
