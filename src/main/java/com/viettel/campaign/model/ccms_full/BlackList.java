package com.viettel.campaign.model.ccms_full;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "CAMPAIGN_BLACKLIST_CAT")
//@Getter
//@Setter
public class BlackList implements Serializable {

    @Id
    @NotNull
    @Column(name = "CAMPAIGN_BLACKLIST_CAT_ID")
    private Long campaignBlacklistCatId;

    @Column(name = "BLACKLIST_NAME")
    private String blackListName;

    @Column(name = "STATUS")
    private Long status;

    @Column(name = "CREATE_BY")
    private Long createBy;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "UPDATE_BY")
    private Long updateBy;

    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    public Long getCampaignBlacklistCatId() {
        return campaignBlacklistCatId;
    }

    public void setCampaignBlacklistCatId(Long campaignBlacklistCatId) {
        this.campaignBlacklistCatId = campaignBlacklistCatId;
    }

    public String getBlackListName() {
        return blackListName;
    }

    public void setBlackListName(String blackListName) {
        this.blackListName = blackListName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
