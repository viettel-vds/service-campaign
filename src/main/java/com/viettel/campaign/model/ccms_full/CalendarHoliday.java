package com.viettel.campaign.model.ccms_full;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "CALENDAR_HOLIDAY", schema = "IPCC_CCMS", catalog = "")
public class CalendarHoliday {
    private int calendarHolidayId;
    private Integer calendarId;
    private Long day;
    private Long month;
    private String name;
    private Boolean status;
    private Integer toDay;
    private Integer toMonth;
    private Integer HolidayType;

    @Id
    @Column(name = "CALENDAR_HOLIDAY_ID")
    public int getCalendarHolidayId() {
        return calendarHolidayId;
    }

    public void setCalendarHolidayId(int calendarHolidayId) {
        this.calendarHolidayId = calendarHolidayId;
    }

    @Basic
    @Column(name = "CALENDAR_ID")
    public Integer getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(Integer calendarId) {
        this.calendarId = calendarId;
    }

    @Basic
    @Column(name = "DAY")
    public Long getDay() {
        return day;
    }

    public void setDay(Long day) {
        this.day = day;
    }

    @Basic
    @Column(name = "MONTH")
    public Long getMonth() {
        return month;
    }

    public void setMonth(Long month) {
        this.month = month;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "STATUS")
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Basic
    @Column(name = "TO_DAY")
    public Integer getToDay() {
        return toDay;
    }

    public void setToDay(Integer toDay) {
        this.toDay = toDay;
    }

    @Basic
    @Column(name = "TO_MONTH")
    public Integer getToMonth() {
        return toMonth;
    }

    public void setToMonth(Integer toMonth) {
        this.toMonth = toMonth;
    }

    @Basic
    @Column(name = "HOLIDAY_TYPE")
    public Integer getHolidayType() {
        return HolidayType;
    }

    public void setHolidayType(Integer HolidayType) {
        this.HolidayType = HolidayType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalendarHoliday that = (CalendarHoliday) o;
        return calendarHolidayId == that.calendarHolidayId &&
                Objects.equals(calendarId, that.calendarId) &&
                Objects.equals(day, that.day) &&
                Objects.equals(month, that.month) &&
                Objects.equals(name, that.name) &&
                Objects.equals(status, that.status) &&
                Objects.equals(toDay, that.toDay) &&
                Objects.equals(toMonth, that.toMonth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(calendarHolidayId, calendarId, day, month, name, status, toDay, toMonth);
    }
}
