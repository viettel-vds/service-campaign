package com.viettel.campaign.model.ccms_full;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "CALENDAR_TIME", schema = "IPCC_CCMS", catalog = "")
public class CalendarTime {
    private int calendarTimeId;
    private Integer calendarId;
    private Boolean day;
    private Long fromHour;
    private Long fromMinute;
    private Long toHour;
    private Long toMinute;
    private Boolean status;
    private Long type;
    private Long fromSecond;
    private Long toSecond;

    @Id
    @Column(name = "CALENDAR_TIME_ID")
    public int getCalendarTimeId() {
        return calendarTimeId;
    }

    public void setCalendarTimeId(int calendarTimeId) {
        this.calendarTimeId = calendarTimeId;
    }

    @Basic
    @Column(name = "CALENDAR_ID")
    public Integer getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(Integer calendarId) {
        this.calendarId = calendarId;
    }

    @Basic
    @Column(name = "DAY")
    public Boolean getDay() {
        return day;
    }

    public void setDay(Boolean day) {
        this.day = day;
    }

    @Basic
    @Column(name = "FROM_HOUR")
    public Long getFromHour() {
        return fromHour;
    }

    public void setFromHour(Long fromHour) {
        this.fromHour = fromHour;
    }

    @Basic
    @Column(name = "FROM_MINUTE")
    public Long getFromMinute() {
        return fromMinute;
    }

    public void setFromMinute(Long fromMinute) {
        this.fromMinute = fromMinute;
    }

    @Basic
    @Column(name = "TO_HOUR")
    public Long getToHour() {
        return toHour;
    }

    public void setToHour(Long toHour) {
        this.toHour = toHour;
    }

    @Basic
    @Column(name = "TO_MINUTE")
    public Long getToMinute() {
        return toMinute;
    }

    public void setToMinute(Long toMinute) {
        this.toMinute = toMinute;
    }

    @Basic
    @Column(name = "STATUS")
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Basic
    @Column(name = "TYPE")
    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    @Basic
    @Column(name = "FROM_SECOND")
    public Long getFromSecond() {
        return fromSecond;
    }

    public void setFromSecond(Long fromSecond) {
        this.fromSecond = fromSecond;
    }

    @Basic
    @Column(name = "TO_SECOND")
    public Long getToSecond() {
        return toSecond;
    }

    public void setToSecond(Long toSecond) {
        this.toSecond = toSecond;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalendarTime that = (CalendarTime) o;
        return calendarTimeId == that.calendarTimeId &&
                Objects.equals(calendarId, that.calendarId) &&
                Objects.equals(day, that.day) &&
                Objects.equals(fromHour, that.fromHour) &&
                Objects.equals(fromMinute, that.fromMinute) &&
                Objects.equals(toHour, that.toHour) &&
                Objects.equals(toMinute, that.toMinute) &&
                Objects.equals(status, that.status) &&
                Objects.equals(type, that.type) &&
                Objects.equals(fromSecond, that.fromSecond) &&
                Objects.equals(toSecond, that.toSecond);
    }

    @Override
    public int hashCode() {
        return Objects.hash(calendarTimeId, calendarId, day, fromHour, fromMinute, toHour, toMinute, status, type, fromSecond, toSecond);
    }
}
