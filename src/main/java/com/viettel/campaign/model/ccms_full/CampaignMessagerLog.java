package com.viettel.campaign.model.ccms_full;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "CAMPAIGN_MESSAGER_LOG", schema = "IPCC_CCMS", catalog = "")
public class CampaignMessagerLog {
    private long messagerIdLog;
    private Long messagerId;
    private String messagerTitle;
    private String mesagerContent;
    private String messagerCustomer;
    private Date messagerDate;
    private String messagerCode;
    private String campaignCode;
    private Boolean isDelete;
    private String messagerTitleNew;
    private String mesagerContentNew;

    @Id
    @NotNull
    @GeneratedValue(generator = "CAMPAIGN_MESSAGER_LOG_SEQ")
    @SequenceGenerator(name = "CAMPAIGN_MESSAGER_LOG_SEQ", sequenceName = "CAMPAIGN_MESSAGER_LOG_SEQ", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "MESSAGER_ID_LOG")
    public long getMessagerIdLog() {
        return messagerIdLog;
    }

    public void setMessagerIdLog(long messagerIdLog) {
        this.messagerIdLog = messagerIdLog;
    }

    @Basic
    @Column(name = "MESSAGER_ID")
    public Long getMessagerId() {
        return messagerId;
    }

    public void setMessagerId(Long messagerId) {
        this.messagerId = messagerId;
    }

    @Basic
    @Column(name = "MESSAGER_TITLE")
    public String getMessagerTitle() {
        return messagerTitle;
    }

    public void setMessagerTitle(String messagerTitle) {
        this.messagerTitle = messagerTitle;
    }

    @Basic
    @Column(name = "MESAGER_CONTENT")
    public String getMesagerContent() {
        return mesagerContent;
    }

    public void setMesagerContent(String mesagerContent) {
        this.mesagerContent = mesagerContent;
    }

    @Basic
    @Column(name = "MESSAGER_CUSTOMER")
    public String getMessagerCustomer() {
        return messagerCustomer;
    }

    public void setMessagerCustomer(String messagerCustomer) {
        this.messagerCustomer = messagerCustomer;
    }

    @Basic
    @Column(name = "MESSAGER_DATE")
    public Date getMessagerDate() {
        return messagerDate;
    }

    public void setMessagerDate(Date messagerDate) {
        this.messagerDate = messagerDate;
    }

    @Basic
    @Column(name = "MESSAGER_CODE")
    public String getMessagerCode() {
        return messagerCode;
    }

    public void setMessagerCode(String messagerCode) {
        this.messagerCode = messagerCode;
    }

    @Basic
    @Column(name = "CAMPAIGN_CODE")
    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    @Basic
    @Column(name = "IS_DELETE")
    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }

    @Basic
    @Column(name = "MESSAGER_TITLE_NEW")
    public String getMessagerTitleNew() {
        return messagerTitleNew;
    }

    public void setMessagerTitleNew(String messagerTitleNew) {
        this.messagerTitleNew = messagerTitleNew;
    }

    @Basic
    @Column(name = "MESAGER_CONTENT_NEW")
    public String getMesagerContentNew() {
        return mesagerContentNew;
    }
    public void setMesagerContentNew(String mesagerContentNew) {
        this.mesagerContentNew = mesagerContentNew;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampaignMessagerLog that = (CampaignMessagerLog) o;
        return messagerIdLog == that.messagerIdLog &&
                Objects.equals(messagerId, that.messagerId) &&
                Objects.equals(messagerTitle, that.messagerTitle) &&
                Objects.equals(mesagerContent, that.mesagerContent) &&
                Objects.equals(messagerCustomer, that.messagerCustomer) &&
                Objects.equals(messagerDate, that.messagerDate) &&
                Objects.equals(messagerCode, that.messagerCode) &&
                Objects.equals(campaignCode, that.campaignCode) &&
                Objects.equals(isDelete, that.isDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messagerIdLog, messagerId, messagerTitle, mesagerContent, messagerCustomer, messagerDate, messagerCode, campaignCode, isDelete);
    }
}
