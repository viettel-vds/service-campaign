package com.viettel.campaign.model.ccms_full;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "CAMPAIGN_BLACKLIST_CONFIG")
@Getter
@Setter
public class CampaignTypeBlackList implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(generator = "CAMPAIGN_BLACKLIST_CONFIG_SEQ")
    @SequenceGenerator(name = "CAMPAIGN_BLACKLIST_CONFIG_SEQ", sequenceName = "CAMPAIGN_BLACKLIST_CONFIG_SEQ", allocationSize = 1)
    @Column(name = "CAMPAIGN_BLACKLIST_CONFIG_ID")
    private Long campaignBlacklistConfigId;

    @Column(name = "CAMPAIGN_TYPE_ID")
    private Long campaignTypeId;

    @Column(name = "CAMPAIGN_BLACKLIST_CAT_ID")
    private Long campaignBlackListCatId;

    @Column(name = "STATUS")
    private Long status;

    @Column(name = "CREATE_BY")
    private Long createBy;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "UPDATE_BY")
    private Long updateBy;

    @Column(name = "UPDATE_DATE")
    private Date updateDate;
}
