package com.viettel.campaign.model.ccms_full;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "CAMPAIGN_MESSAGER", schema = "IPCC_CCMS", catalog = "")
public class CampaignMessager {
    private long messagerId;
    private String messagerTitle;
    private String messagerTitleExt;
    private String mesagerContent;
    private String messagerCustomer;
    private String campaignCode;
    private Date messagerDate;
    private Boolean isDelete;
    private String messagerCode;
    private String campaignName;
    private Long campaignId;
    @Id
    @NotNull
    @GeneratedValue(generator = "CAMPAIGN_MESSAGER_SEQ")
    @SequenceGenerator(name = "CAMPAIGN_MESSAGER_SEQ", sequenceName = "CAMPAIGN_MESSAGER_SEQ", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "MESSAGER_ID")
    public long getMessagerId() {
        return messagerId;
    }

    public void setMessagerId(long messagerId) {
        this.messagerId = messagerId;
    }

    @Basic
    @Column(name = "MESSAGER_TITLE_EXT")
    public String getMessagerTitleExt() {
        return messagerTitleExt;
    }

    public void setMessagerTitleExt(String messagerTitleExt) {
        this.messagerTitleExt = messagerTitleExt;
    }

    @Basic
    @Column(name = "MESSAGER_TITLE")
    public String getMessagerTitle() {
        return messagerTitle;
    }

    public void setMessagerTitle(String messagerTitle) {
        this.messagerTitle = messagerTitle;
    }

    @Basic
    @Column(name = "MESAGER_CONTENT")
    public String getMesagerContent() {
        return mesagerContent;
    }

    public void setMesagerContent(String mesagerContent) {
        this.mesagerContent = mesagerContent;
    }

    @Basic
    @Column(name = "MESSAGER_CUSTOMER")
    public String getMessagerCustomer() {
        return messagerCustomer;
    }

    public void setMessagerCustomer(String messagerCustomer) {
        this.messagerCustomer = messagerCustomer;
    }

    @Basic
    @Column(name = "CAMPAIGN_CODE")
    public String getCampaignCode() {
        return campaignCode;
    }
    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    @Basic
    @Column(name = "CAMPAIGN_NAME")
    public String getCampaignName() {
        return campaignName;
    }
    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    @Basic
    @Column(name="MESSAGER_DATE")
    public Date getMessagerDate(){return messagerDate;}
    public void setMessagerDate(Date messagerDate) {
        this.messagerDate = messagerDate;
    }

    @Basic
    @Column(name="IS_DELETE")
    public Boolean getIsDelete(){return isDelete;}
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    @Basic
    @Column(name="MESSAGER_CODE")
    public String getMessagerCode(){return messagerCode;}
    public void setMessagerCode(String messagerCode) {
        this.messagerCode = messagerCode;
    }

    @Basic
    @Column(name = "CAMPAIGN_ID")
    public long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(messagerId, messagerTitle, mesagerContent, messagerCustomer);
    }
}
