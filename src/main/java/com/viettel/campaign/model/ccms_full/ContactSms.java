package com.viettel.campaign.model.ccms_full;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
@Entity
@Table(name = "CONTACT_SMS")
public class ContactSms implements Serializable {
    @Id
    @NotNull
    @GeneratedValue(generator = "CONTACT_SMS_SEQ")
    @SequenceGenerator(name = "CONTACT_SMS_SEQ", sequenceName = "CONTACT_SMS_SEQ", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "CONTACT_SMS_ID")
    private Long contactSmsId;
    @Column(name = "CAMPAIGN_ID")
    private Long campaignId;
    @Column(name = "SMS_CODE")
    private String smsCode;
    @Column(name = "SMS_NAME")
    private String smsName;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "CREATE_BY")
    private String createBy;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_TIME")
    private Date createTime;
    @Column(name = "UPDATE_BY")
    private String updateBy;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    public Long getContactSmsId() {
        return contactSmsId;
    }

    public void setContactSmsId(Long contactSmsId) {
        this.contactSmsId = contactSmsId;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }

    public String getSmsName() {
        return smsName;
    }

    public void setSmsName(String smsName) {
        this.smsName = smsName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
