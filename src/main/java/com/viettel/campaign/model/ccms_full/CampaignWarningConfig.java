package com.viettel.campaign.model.ccms_full;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "CAMPAIGN_WARNING_CONFIG")
@Getter
@Setter
public class CampaignWarningConfig implements Serializable {
    @Id
    @NotNull
    @GeneratedValue(generator = "CAMPAIGN_WARNING_CONFIG_SEQ")
    @SequenceGenerator(name = "CAMPAIGN_WARNING_CONFIG_SEQ", sequenceName = "CAMPAIGN_WARNING_CONFIG_SEQ", allocationSize = 1)
    @Column(name = "ID")
    private Long Id;

    @Column(name = "COLOR")
    private String color ;

    @Column(name = "LEVEL_START")
    private Long levelStart;

    @Column(name = "LEVEL_END")
    private Long levelEnd;

    @Column(name = "STATUS")
    private Long status;

    @Column(name = "CAMPAIGN_ID")
    private Long campaignId;

    @Column(name = "CREATE_BY")
    private String createBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_TIME")
    private Date createTime;

    @Column(name = "MODIFY_BY")
    private String modifyBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MODIFY_TIME")
    private Date modifyTime;
}
