package com.viettel.campaign.service;

import com.viettel.campaign.model.ccms_full.CampaignWarningConfig;
import com.viettel.campaign.model.ccms_full.CampaignWarningDefault;
import com.viettel.campaign.web.dto.CampaignCustomerDTO;
import com.viettel.campaign.web.dto.CampaignDTO;
import com.viettel.campaign.web.dto.CampaignWarningConfigDTO;
import com.viettel.campaign.web.dto.ResultDTO;

import java.text.ParseException;
import java.util.List;

public interface CampaignWarningConfigService {
    ResultDTO addNewCampaignWarningConfig(List<CampaignWarningConfigDTO> campaignWarningConfigDTO) throws ParseException;

    Iterable<CampaignWarningConfig> getAllConfigByCampaignId(Long campaignId);

    ResultDTO deleteCampaignConfigById(Long campaignId) throws ParseException;
}
