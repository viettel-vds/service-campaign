//package com.viettel.campaign.service;
//
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
//
///**
// * @author hanv_itsol
// * @project service-campaign
// */
//@Service
//public class Consumer {
//    @KafkaListener(topics = "TestTopic", groupId = "1001")
//    public void consume(String message){
//        System.out.println("Consumed Message: " + message);
//    }
//}
