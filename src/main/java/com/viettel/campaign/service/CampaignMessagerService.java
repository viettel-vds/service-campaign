package com.viettel.campaign.service;


import com.viettel.campaign.web.dto.CampaignMessagerDTO;
import com.viettel.campaign.web.dto.ResultDTO;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

public interface CampaignMessagerService {
    ResultDTO addNewCampaignMessager(CampaignMessagerDTO CampaignMessagerDTO,HttpServletRequest request);
    ResultDTO UpsetCampaignMessager(CampaignMessagerDTO CampaignMessagerDTO,HttpServletRequest request);
    ResultDTO getCampaignMessenger(CampaignMessagerDTO CampaignMessagerDTO);
    ResultDTO getCampaignMessengerExt(Integer currentPage, Integer perPage,CampaignMessagerDTO requestDto);
    ResultDTO getCampaignMessengerLog(Integer currentPage, Integer perPage, Long messagerId, Integer timezoneOffset);
    ResultDTO delCampaignMessager(Long messagerId);
    ResultDTO GetIDCampaignMessager(Long messagerId);
    Map<String, Object> readAndValidateCustomer(String path, Long campaignId, String campaignCode, String campaignName, String language, HttpServletRequest httpServletRequest) throws IOException,Exception ;
    XSSFWorkbook buildTemplate(String language) throws IOException;
}
