package com.viettel.campaign.service;

import com.viettel.campaign.model.ccms_full.CampaignWarningDefault;
import com.viettel.campaign.web.dto.ResultDTO;

public interface CampaignWarningDefaultService {
    Iterable<CampaignWarningDefault> getAllParams();
}
