package com.viettel.campaign.service.impl;

import com.viettel.campaign.config.DataSourceQualify;
import com.viettel.campaign.model.ccms_full.ApParam;
import com.viettel.campaign.model.ccms_full.CampaignWarningDefault;
import com.viettel.campaign.repository.ccms_full.CampaignWarningDefaultRepository;
import com.viettel.campaign.service.CampaignWarningDefaultService;
import com.viettel.campaign.web.dto.ResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CampaignWarningDefaultImpl implements CampaignWarningDefaultService {
    private static final Logger logger = LoggerFactory.getLogger(ApParamServiceImpl.class);

    @Autowired
    CampaignWarningDefaultRepository campaignWarningDefaultRepository;

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public Iterable<CampaignWarningDefault> getAllParams() {
        return campaignWarningDefaultRepository.findAll();
    }
}
