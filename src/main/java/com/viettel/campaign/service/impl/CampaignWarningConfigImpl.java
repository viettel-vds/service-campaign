package com.viettel.campaign.service.impl;

import com.viettel.campaign.config.DataSourceQualify;
import com.viettel.campaign.model.ccms_full.*;
import com.viettel.campaign.repository.ccms_full.CampaignWarningConfigRepository;
import com.viettel.campaign.repository.ccms_full.CampaignWarningDefaultRepository;
import com.viettel.campaign.service.CampaignWarningConfigService;
import com.viettel.campaign.service.CampaignWarningDefaultService;
import com.viettel.campaign.utils.Config;
import com.viettel.campaign.utils.Constants;
import com.viettel.campaign.utils.TimeZoneUtils;
import com.viettel.campaign.web.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CampaignWarningConfigImpl implements CampaignWarningConfigService {
    private static final Logger logger = LoggerFactory.getLogger(ApParamServiceImpl.class);

    @Autowired
    CampaignWarningConfigRepository campaignWarningConfigRepository;

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public ResultDTO addNewCampaignWarningConfig(List<CampaignWarningConfigDTO> campaignWarningConfigDTO) throws ParseException {
        ResultDTO resultDTO = new ResultDTO();
        List<CampaignWarningConfig> lstCampaignWarningConfig = new ArrayList<>();
        for (CampaignWarningConfigDTO item : campaignWarningConfigDTO) {
            lstCampaignWarningConfig.add(item.toEntity());
        }
        try {
            List<CampaignWarningConfig> dataReturn = new ArrayList<>();
            for (int i = 0; i < lstCampaignWarningConfig.size(); i++) {
                CampaignWarningConfig item = lstCampaignWarningConfig.get(i);
                item.setCreateTime(new Date());
                campaignWarningConfigRepository.save(item);
                dataReturn.add(item);
            }
            resultDTO.setErrorCode(Constants.ApiErrorCode.SUCCESS);
            resultDTO.setDescription(Constants.ApiErrorDesc.SUCCESS);
            resultDTO.setListData(dataReturn);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return resultDTO;
    }

    @Override
    public Iterable<CampaignWarningConfig> getAllConfigByCampaignId(Long campaignId) {
        return campaignWarningConfigRepository.findAllByCampaignId(campaignId);
    }

    @Override
    public ResultDTO deleteCampaignConfigById(Long campaignId) throws ParseException {
        ResultDTO resultDTO = new ResultDTO();
        try {
            if (campaignId != null) {
                // delete
                campaignWarningConfigRepository.removeByCampaignId(campaignId);

                resultDTO.setErrorCode(Constants.ApiErrorCode.SUCCESS);
                resultDTO.setDescription(Constants.ApiErrorDesc.SUCCESS);
            }
            else {
                resultDTO.setErrorCode(Constants.ApiErrorCode.ERROR);
                resultDTO.setDescription(Constants.ApiErrorDesc.ERROR);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return resultDTO;
    }
}