package com.viettel.campaign.service.impl;
import com.viettel.campaign.config.DataSourceQualify;
import com.viettel.campaign.model.ccms_full.CampaignMessager;
import com.viettel.campaign.model.ccms_full.CampaignMessagerLog;
import com.viettel.campaign.repository.ccms_full.CampaignMessagerLogReponsitory;
import com.viettel.campaign.repository.ccms_full.CampaignMessagerRepository;
import com.viettel.campaign.service.CampaignMessagerService;
import com.viettel.campaign.utils.Constants;
import com.viettel.campaign.utils.HibernateUtil;
import com.viettel.campaign.utils.RedisUtil;
import com.viettel.campaign.web.dto.CampaignMessagerDTO;
import com.viettel.campaign.web.dto.ResultDTO;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.provider.HibernateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.viettel.econtact.filter.UserSession;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.file.Files;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

@Service
@Transactional(rollbackFor = Exception.class)
public class CampaignMessagerServiceImpl implements CampaignMessagerService {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    CampaignMessagerRepository campaignMessagerRepository;
    @Autowired
    CampaignMessagerLogReponsitory campaignMessagerLogRepository;
    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public ResultDTO addNewCampaignMessager(@NotNull CampaignMessagerDTO CampaignMessagerDTO,HttpServletRequest request) {
        String xAuthToken = request.getHeader("X-Auth-Token");
        UserSession userSession = (UserSession) RedisUtil.getInstance().get(xAuthToken);
        CampaignMessagerDTO.setMessagerTitleExt(removeAccent(CampaignMessagerDTO.getMessagerTitle().toLowerCase()));
        CampaignMessagerDTO.setIsDelete(false);
        CampaignMessagerDTO.setMessagerCustomer(userSession.getUserName());
        CampaignMessagerDTO.setMessagerDate(new Date());
        String Prefix=CampaignMessagerDTO.getCampaignId()+"_";
        String lastid=campaignMessagerRepository.getLastID(Prefix);
        CampaignMessagerDTO.setMessagerCode(NextID(lastid,Prefix));

        ResultDTO resultDTO = new ResultDTO();
        try
        {
            CampaignMessager campaignMessager = modelMapper.map(CampaignMessagerDTO, CampaignMessager.class);
            CampaignMessager campaignResult = campaignMessagerRepository.save(campaignMessager);
            //CampaignMessagerLog campaignMessagerLog = modelMapper.map(CampaignMessagerDTO, CampaignMessagerLog.class);
            //campaignMessagerLog.setMessagerId(campaignResult.getMessagerId());
            //campaignMessagerLogRepository.save(campaignMessagerLog);
            resultDTO.setErrorCode(Constants.ApiErrorCode.SUCCESS);
            resultDTO.setDescription(Constants.ApiErrorDesc.SUCCESS);
            resultDTO.setData(campaignResult);
        }
        catch (Exception ex)
        {
            resultDTO.setErrorCode(Constants.ApiErrorCode.ERROR);
            resultDTO.setDescription(Constants.ApiErrorDesc.ERROR);
        }
        return resultDTO;
    }

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public ResultDTO UpsetCampaignMessager(CampaignMessagerDTO CampaignMessagerDTO ,HttpServletRequest request) {
        ResultDTO resultDTO = new ResultDTO();
        String xAuthToken = request.getHeader("X-Auth-Token");
        UserSession userSession = (UserSession) RedisUtil.getInstance().get(xAuthToken);
        CampaignMessager campaignMessagerInID = campaignMessagerRepository.findByMessagerId(CampaignMessagerDTO.getMessagerId());
        if(campaignMessagerInID==null) return null;
        try{
            CampaignMessager campaignMessager = modelMapper.map(CampaignMessagerDTO, CampaignMessager.class);
            campaignMessager.setMessagerTitleExt(removeAccent(campaignMessager.getMessagerTitle().toLowerCase()));
            campaignMessager.setMessagerCustomer(userSession.getUserName());
            campaignMessager.setCampaignId(campaignMessagerInID.getCampaignId());
            campaignMessager.setCampaignCode(campaignMessagerInID.getCampaignCode());
            campaignMessager.setMessagerDate(new Date());


            CampaignMessagerLog campaignMessagerLog = modelMapper.map(CampaignMessagerDTO, CampaignMessagerLog.class);
            campaignMessagerLog.setMessagerId(campaignMessager.getMessagerId());
            campaignMessagerLog.setMessagerCustomer(userSession.getUserName());
            campaignMessagerLog.setMessagerDate(new Date());
            campaignMessagerLog.setMessagerTitleNew(campaignMessagerInID.getMessagerTitle());
            campaignMessagerLog.setMesagerContentNew(campaignMessagerInID.getMesagerContent());

            CampaignMessager campaignResult = campaignMessagerRepository.save(campaignMessager);
            campaignMessagerLogRepository.save(campaignMessagerLog);

            resultDTO.setErrorCode(Constants.ApiErrorCode.SUCCESS);
            resultDTO.setDescription(Constants.ApiErrorDesc.SUCCESS);
            resultDTO.setData(campaignResult);
        }
        catch (Exception ex)
        {
            resultDTO.setErrorCode(Constants.ApiErrorCode.ERROR);
            resultDTO.setDescription(Constants.ApiErrorDesc.ERROR);
        }
        return resultDTO;
    }

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public ResultDTO getCampaignMessenger(CampaignMessagerDTO CampaignMessagerDTO) {
        ResultDTO resultDTO = new ResultDTO();
        resultDTO= campaignMessagerRepository.getCampaignMessagerList(CampaignMessagerDTO);

        return resultDTO;
    }

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public ResultDTO getCampaignMessengerExt(Integer currentPage, Integer perPage,CampaignMessagerDTO requestDto) {
        ResultDTO resultDTO = new ResultDTO();
        resultDTO= campaignMessagerRepository.getCampaignMessagerListExt(currentPage,perPage,requestDto);
        return resultDTO;
    }

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public ResultDTO getCampaignMessengerLog(Integer currentPage, Integer perPage, Long messagerId, Integer timezoneOffset) {
        ResultDTO resultDTO = new ResultDTO();
        resultDTO= campaignMessagerRepository.getCampaignMessagerLogList(currentPage,perPage,messagerId, timezoneOffset);
        return resultDTO;
    }

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public ResultDTO delCampaignMessager(Long messagerId) {
        ResultDTO resultDTO = new ResultDTO();
        try{
            CampaignMessager campaignResult = campaignMessagerRepository.findByMessagerId(messagerId);
            campaignResult.setIsDelete(true);
            campaignResult =campaignMessagerRepository.save(campaignResult);

            resultDTO.setErrorCode(Constants.ApiErrorCode.SUCCESS);
            resultDTO.setDescription(Constants.ApiErrorDesc.SUCCESS);
            resultDTO.setData(campaignResult);
        }
        catch (Exception ex)
        {
            resultDTO.setErrorCode(Constants.ApiErrorCode.DELETE_ERROR);
            resultDTO.setDescription(Constants.ApiErrorDesc.DELETE_ERROR);
        }
        return resultDTO;
    }

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public ResultDTO GetIDCampaignMessager(Long messagerId) {
        ResultDTO resultDTO = new ResultDTO();
        CampaignMessager campaignResult = campaignMessagerRepository.findByMessagerId(messagerId);
        resultDTO.setErrorCode(Constants.ApiErrorCode.SUCCESS);
        resultDTO.setDescription(Constants.ApiErrorDesc.SUCCESS);
        resultDTO.setData(campaignResult);
        return resultDTO;
    }

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public  Map<String, Object> readAndValidateCustomer(String path,Long campaignId,String campaignCode,String campaignName, String language, HttpServletRequest httpServletRequest) throws IOException, Exception
    {
        Map<String, Object> result = new HashMap<>();
        XSSFWorkbook workbook = null;
        Locale locale = Locale.forLanguageTag(language);
        DataFormatter dataFormat = new DataFormatter();
        List<String> stringHeader = new ArrayList<>();
        List<CampaignMessagerDTO> listms= new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy", Locale.ENGLISH);
        formatter.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        String Prefix=null;
        String lastid=null;
        int objectSize;
        String xAuthToken = httpServletRequest.getHeader("X-Auth-Token");
        UserSession userSession = (UserSession) RedisUtil.getInstance().get(xAuthToken);
        try {
            File file = new File(path);
            FileInputStream fis = new FileInputStream(file);
            workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Row row = sheet.getRow(3);

            List<Object[]> rawDataList = new ArrayList<>();

            //<editor-fold desc="Kiểm tra file dữ liệu rỗng" defaultstate="collapsed">
            objectSize = row.getLastCellNum();
            //</editor-fold>

            int x=sheet.getPhysicalNumberOfRows();

            //<editor-fold desc="Đọc dữ liệu từng dòng 1" defaultstate="collapsed">
            for (int i = 4; i < sheet.getPhysicalNumberOfRows(); i++) {
                Row dataRow = sheet.getRow(i);
                if (!isRowEmpty(dataRow)) {
                    CampaignMessager item= new CampaignMessager();
                    //set messager
                    item.setMessagerTitle(dataFormat.formatCellValue( dataRow.getCell(1)).toString().trim());
                    item.setMesagerContent(dataFormat.formatCellValue( dataRow.getCell(2)).toString().trim());
                    item.setMessagerCustomer(userSession.getUserName());
                    //item.setStartDate(formatter.parse(dataRow.getCell(2).toString().trim()));
                    //item.setEndDate(formatter.parse(dataRow.getCell(3).toString().trim()));
                    item.setMessagerTitleExt(removeAccent(item.getMessagerTitle().toLowerCase()));
                    item.setMessagerDate(new Date());
                    item.setIsDelete(false);
                    Prefix=campaignId.toString()+"_";
                    if(lastid==null){lastid=campaignMessagerRepository.getLastID(Prefix);}
                    //lastid=campaignMessagerRepository.getLastID(Prefix);
                    item.setMessagerCode(NextID(lastid,Prefix));
                    //set campaign
                    item.setCampaignId(campaignId);
                    item.setCampaignCode(campaignCode);
                    item.setCampaignName(campaignName);
                    if(item.getMessagerTitle()!=""&&item.getMesagerContent()!=""&&item.getMesagerContent().length()<=1000)
                    {
                        //boolean ck= campaignMessagerRepository.InsertMessager(item);
                        //CampaignMessagerDTO campaignMessagersaved = campaignMessagerRepository.GetMessagerInId(item.getMessagerCode());
                        //if(ck)listms.add(item);
                        CampaignMessager cm =campaignMessagerRepository.save(item);
                        lastid=cm.getMessagerCode();
                    }
                    if(i > 1004) item.setIsDelete(true);
                    listms.add(modelMapper.map(item, CampaignMessagerDTO.class));
                } else break;
            }
            byte[] content =buildResultTemplate(listms,language);
            result.put("content", content);
            result.put("message", "Success");
            return  result;
        }
        catch (Exception ex) {
            result.put("content", null);
            result.put("message", "Error");
            return  result;
        }
    }

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public XSSFWorkbook buildTemplate(String language) throws IOException {
        Locale locale = Locale.forLanguageTag(language);
        XSSFWorkbook workbook = null;
        try {
            workbook = new XSSFWorkbook();
            CreationHelper creationHelper = workbook.getCreationHelper();
            //ByteArrayOutputStream os = new ByteArrayOutputStream();
            Sheet sheet = workbook.createSheet("IMPORT");
            DataFormat dataFormat = workbook.createDataFormat();


            //<editor-fold desc="Tạo style và font" defaultstate="collapsed">
            Font headerFont = workbook.createFont();
            Font importantFont = workbook.createFont();
            importantFont.setColor(IndexedColors.RED.index);
            headerFont.setBold(true);
            CellStyle headerStyle = workbook.createCellStyle();
            CellStyle importantStyle = workbook.createCellStyle();
            CellStyle columnStyle = workbook.createCellStyle();
            importantStyle.setFont(importantFont);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            headerStyle.setFont(headerFont);
            columnStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            columnStyle.setAlignment(HorizontalAlignment.CENTER);
            columnStyle.setWrapText(true);
            columnStyle.setDataFormat(dataFormat.getFormat("@"));
            //</editor-fold>

            //<editor-fold desc="Thêm combobox" defaultstate="collapsed">

            //</editor-fold>

            //<editor-fold desc="Ghi header" defaultstate="collapsed">
            Drawing drawing = sheet.createDrawingPatriarch();
            ClientAnchor anchor = creationHelper.createClientAnchor();
            Row row0 = sheet.createRow(0);
            Row row2 = sheet.createRow(2);
            Row row3 = sheet.createRow(3);
            Cell cell0 = row0.createCell(0);
            cell0.setCellStyle(headerStyle);
            cell0.setCellValue("IMPORT MESSAGER");
            Cell cell2 = row2.createCell(0);
            cell2.setCellStyle(importantStyle);
            cell2.setCellValue("Chú ý: 1 bản ghi được coi là hợp lệ bắt buộc phải có đầy đủ thông tin");
            ///////0
            sheet.setDefaultColumnStyle(0, columnStyle);
            Cell headerCell0 = row3.createCell(0);
            headerCell0.setCellValue("STT");
            headerStyle.setBorderTop(BorderStyle.THIN);
            headerStyle.setBorderRight(BorderStyle.THIN);
            headerStyle.setBorderBottom(BorderStyle.THIN);
            headerStyle.setBorderLeft(BorderStyle.THIN);
            headerFont.setFontHeightInPoints((short) 11);
            headerCell0.setCellStyle(headerStyle);
            sheet.setColumnWidth(0, 3500);
            //1
            sheet.setDefaultColumnStyle(1, columnStyle);
            Cell headerCell1 = row3.createCell(1);
            headerCell1.setCellValue("Tên tin nhắn");
            headerStyle.setBorderTop(BorderStyle.THIN);
            headerStyle.setBorderRight(BorderStyle.THIN);
            headerStyle.setBorderBottom(BorderStyle.THIN);
            headerStyle.setBorderLeft(BorderStyle.THIN);
            headerFont.setFontHeightInPoints((short) 11);
            headerCell1.setCellStyle(headerStyle);
            sheet.setColumnWidth(1, 5500);

            //2
            sheet.setDefaultColumnStyle(2, columnStyle);
            Cell headerCell2 = row3.createCell(2);
            headerCell2.setCellValue("Nội dung tin nhắn");
            headerStyle.setBorderTop(BorderStyle.THIN);
            headerStyle.setBorderRight(BorderStyle.THIN);
            headerStyle.setBorderBottom(BorderStyle.THIN);
            headerStyle.setBorderLeft(BorderStyle.THIN);
            headerFont.setFontHeightInPoints((short) 11);
            headerCell2.setCellStyle(headerStyle);
            sheet.setColumnWidth(2, 7000);

            //</editor-fold>

            sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 3));
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 3));
            return workbook;
        } catch (Exception e) {
            return null;
        } finally {
//            if (workbook != null) workbook.close();
        }
    }

    private byte[] buildResultTemplate(List<CampaignMessagerDTO> campaignMessager,String language)
    {
        Locale locale = Locale.forLanguageTag(language);
        XSSFWorkbook workbook = null;
        workbook = new XSSFWorkbook();
        CreationHelper creationHelper = workbook.getCreationHelper();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        OutputStream outputStream;
        Sheet sheet = workbook.createSheet("IMPORT");
        DataFormat dataFormat = workbook.createDataFormat();

        //<editor-fold desc="Tạo style và font" defaultstate="collapsed">
        Font headerFont = workbook.createFont();
        Font cellFont = workbook.createFont();
        Font importantFont = workbook.createFont();
        importantFont.setColor(IndexedColors.RED.index);
        headerFont.setBold(true);
        CellStyle headerStyle = workbook.createCellStyle();
        CellStyle cellStyle = workbook.createCellStyle();
        CellStyle importantStyle = workbook.createCellStyle();
        CellStyle columnStyle = workbook.createCellStyle();
        importantStyle.setFont(importantFont);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerStyle.setFont(headerFont);
        columnStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        columnStyle.setAlignment(HorizontalAlignment.CENTER);
        columnStyle.setWrapText(true);
        columnStyle.setDataFormat(dataFormat.getFormat("@"));
        // set cell body
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        //
        Drawing drawing = sheet.createDrawingPatriarch();
        ClientAnchor anchor = creationHelper.createClientAnchor();
        Row row0 = sheet.createRow(0);
        Row row2 = sheet.createRow(2);
        Row row3 = sheet.createRow(3);
        Cell cell0 = row0.createCell(0);
        cell0.setCellStyle(headerStyle);
        cell0.setCellValue("IMPORT MESSAGER");
        Cell cell2 = row2.createCell(0);
        cell2.setCellStyle(importantStyle);
        cell2.setCellValue("Chú ý: 1 bản ghi được coi là hợp lệ bắt buộc phải có đầy đủ thông tin");
        ///////0
        sheet.setDefaultColumnStyle(0, columnStyle);
        Cell headerCell0 = row3.createCell(0);
        headerCell0.setCellValue("STT");
        headerStyle.setBorderTop(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerFont.setFontHeightInPoints((short) 11);
        headerCell0.setCellStyle(headerStyle);
        sheet.setColumnWidth(0, 3500);
        //1
        sheet.setDefaultColumnStyle(1, columnStyle);
        Cell headerCell1 = row3.createCell(1);
        headerCell1.setCellValue("Tên tin nhắn");
        headerStyle.setBorderTop(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerFont.setFontHeightInPoints((short) 11);
        headerCell1.setCellStyle(headerStyle);
        sheet.setColumnWidth(1, 5500);
        //2
        sheet.setDefaultColumnStyle(2, columnStyle);
        Cell headerCell2 = row3.createCell(2);
        headerCell2.setCellValue("Nội dung tin nhắn");
        headerStyle.setBorderTop(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerFont.setFontHeightInPoints((short) 11);
        headerCell2.setCellStyle(headerStyle);
        sheet.setColumnWidth(2, 7000);
        //3
        sheet.setDefaultColumnStyle(2, columnStyle);
        Cell headerCell3 = row3.createCell(3);
        headerCell3.setCellValue("Trạng thái");
        headerStyle.setBorderTop(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerFont.setFontHeightInPoints((short) 11);
        headerCell3.setCellStyle(headerStyle);
        sheet.setColumnWidth(2, 7000);
        //</editor-fold>

        //
        int index=3;
        int stt=0;

        for(int i=0;i<campaignMessager.size();i++)
        {
            index++;
            stt++;
            Row row = sheet.createRow(index);
            ///////0
            sheet.setDefaultColumnStyle(0, columnStyle);
            headerCell0 = row.createCell(0);
            headerCell0.setCellValue(stt);

            cellFont.setFontHeightInPoints((short) 11);
            headerCell0.setCellStyle(cellStyle);
            sheet.setColumnWidth(0, 3500);
            //1
            sheet.setDefaultColumnStyle(1, columnStyle);
            headerCell1 = row.createCell(1);
            headerCell1.setCellValue(campaignMessager.get(i).getMessagerTitle());

            cellFont.setFontHeightInPoints((short) 11);
            headerCell1.setCellStyle(cellStyle);
            sheet.setColumnWidth(1, 5500);
            //2
            sheet.setDefaultColumnStyle(2, columnStyle);
            headerCell2 = row.createCell(2);
            headerCell2.setCellValue(campaignMessager.get(i).getMesagerContent());

            cellFont.setFontHeightInPoints((short) 11);
            headerCell2.setCellStyle(cellStyle);
            sheet.setColumnWidth(2, 7000);
            //3
            sheet.setDefaultColumnStyle(3, columnStyle);
            headerCell3 = row.createCell(3);
            String error="";
            if (campaignMessager.get(i).getMessagerTitle()=="")error="Tên tin nhắn để trống\n";
            if (campaignMessager.get(i).getMesagerContent()=="")error+="Nội dung tin nhắn để trống\n";
            if (campaignMessager.get(i).getMesagerContent().length()>1000 )error+="Nội dung tin nhắn quá 1000 kí tự\n";
            if (campaignMessager.get(i).getIsDelete()==true )error="False";
            if(error=="")headerCell3.setCellValue("Import thành công");
            else headerCell3.setCellValue(error);

            cellFont.setFontHeightInPoints((short) 11);
            headerCell3.setCellStyle(cellStyle);
            sheet.setColumnWidth(2, 7000);
        }
        //


        sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 3));
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 3));

        try {
            workbook.write(os);
            os.flush();
            os.close();
            return os.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return  null;
        }
        //return workbook;
    }

    //<editor-fold desc="Validate Methods" defaultstate="collapsed">
    private boolean isRowEmpty(Row row) {
        for (int i = 0; i <= row.getLastCellNum(); i++) {
            Cell cell = row.getCell(i);
            if (cell != null && cell.getCellType() != CellType.BLANK) {
                return false;
            }
        }
        return true;
    }

    private String NextID(String lastID,String prefixID){
        if (lastID == "" || lastID == null)
        {
            return prefixID + "1";  // fixwidth default
        }
        Integer nextID = Integer.parseInt(lastID.replaceFirst(prefixID, "")) + 1;
        Integer lengthNumerID = lastID.length() - prefixID.length();
        String zeroNumber = "";
        for (int i = 1; i <= lengthNumerID; i++)
        {
            if (nextID < Math.pow(10, i))
            {
                for (int j = 1; j <= lengthNumerID - i; i++)
                {
                    zeroNumber += "0";
                }
                return prefixID + zeroNumber + nextID;
            }
        }
        return prefixID + nextID;
    }
    public static String removeAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }
}
