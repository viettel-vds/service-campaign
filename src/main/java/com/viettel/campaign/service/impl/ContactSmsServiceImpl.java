package com.viettel.campaign.service.impl;

import com.viettel.campaign.config.DataSourceQualify;
import com.viettel.campaign.model.ccms_full.ContactSms;
import com.viettel.campaign.repository.ccms_full.ContactSmsRepository;
import com.viettel.campaign.service.ContactSmsService;
import com.viettel.campaign.utils.Constants;
import com.viettel.campaign.web.dto.ResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContactSmsServiceImpl implements ContactSmsService {
    private static final Logger logger = LoggerFactory.getLogger(ContactSmsServiceImpl.class);

    @Autowired
    ContactSmsRepository contactSmsRepository;

    @Override
    @Transactional(DataSourceQualify.CCMS_FULL)
    public ResultDTO getAllContactSmsPage(int page, int pageSize, String sort) {
        ResultDTO resultDTO = new ResultDTO();
        try {
            Page<ContactSms> list = contactSmsRepository.findAll(PageRequest.of(page, pageSize));
            resultDTO.setListData(list.getContent());
            resultDTO.setTotalRow(list.getTotalElements());
            resultDTO.setErrorCode(Constants.ApiErrorCode.SUCCESS);
            resultDTO.setDescription(Constants.ApiErrorDesc.SUCCESS);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultDTO.setErrorCode(Constants.ApiErrorCode.ERROR);
            resultDTO.setDescription(Constants.ApiErrorDesc.ERROR);
        }

        return resultDTO;
    }

    @Override
    public ResultDTO findBySmsName(String smsName) {
        ResultDTO resultDTO = new ResultDTO();
        try {
            List<ContactSms> lst = contactSmsRepository.findBySmsNameLike("%" + smsName + "%");
            resultDTO.setListData(lst);
            resultDTO.setTotalRow(Long.valueOf(lst.size()));
            resultDTO.setErrorCode(Constants.ApiErrorCode.SUCCESS);
            resultDTO.setDescription(Constants.ApiErrorDesc.SUCCESS);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultDTO.setErrorCode(Constants.ApiErrorCode.ERROR);
            resultDTO.setDescription(Constants.ApiErrorDesc.ERROR);
        }
        return resultDTO;
    }
}
