package com.viettel.campaign.service;

import com.viettel.campaign.web.dto.ResultDTO;


public interface ContactSmsService {
    ResultDTO getAllContactSmsPage(int page, int pageSize, String sort);

    ResultDTO findBySmsName(String smsName);

}
