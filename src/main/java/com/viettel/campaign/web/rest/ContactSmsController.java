package com.viettel.campaign.web.rest;

import com.viettel.campaign.service.ContactSmsService;
import com.viettel.campaign.web.dto.ResultDTO;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/ipcc/contactSms")
@CrossOrigin(origins = "*")
public class ContactSmsController {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ContactSmsController.class);

    @Autowired
    ContactSmsService contactSmsService;

    @GetMapping(path = "/findAlls")
    @ResponseBody
    public ResponseEntity<ResultDTO> listAllContactSms(@RequestParam("page") int page, @RequestParam("pageSize") int pageSize, @RequestParam("sort") String sort) {
        ResultDTO result = contactSmsService.getAllContactSmsPage(page, pageSize, sort);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/searchContactSms")
    public ResponseEntity<ResultDTO> searchContractSms(@RequestParam String smsName) {
        ResultDTO result = contactSmsService.findBySmsName(smsName);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
