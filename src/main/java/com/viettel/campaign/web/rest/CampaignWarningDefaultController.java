package com.viettel.campaign.web.rest;

import com.viettel.campaign.model.ccms_full.ApParam;
import com.viettel.campaign.model.ccms_full.CampaignWarningDefault;
import com.viettel.campaign.service.ApParamService;
import com.viettel.campaign.service.CampaignExecuteService;
import com.viettel.campaign.service.CampaignWarningDefaultService;
import com.viettel.campaign.web.dto.ResultDTO;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/ipcc/capwd")
@CrossOrigin(origins = "*")
public class CampaignWarningDefaultController {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CampaignWarningDefaultController.class);

    @Autowired(required = true)
    CampaignWarningDefaultService campaignWarningDefaultService;

    @GetMapping(path = "/findAllCampaignWarningDefault")
    @ResponseBody
    public ResponseEntity listAllCampaignWarningDefault() {
        Iterable<CampaignWarningDefault> listCampaignWarningDefault = campaignWarningDefaultService.getAllParams();
        if (listCampaignWarningDefault == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity(listCampaignWarningDefault.iterator(), HttpStatus.OK);

    }
}

