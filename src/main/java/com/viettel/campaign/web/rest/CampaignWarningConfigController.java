package com.viettel.campaign.web.rest;

import com.viettel.campaign.model.ccms_full.CampaignWarningConfig;
import com.viettel.campaign.model.ccms_full.CampaignWarningDefault;
import com.viettel.campaign.service.CampaignWarningConfigService;
import com.viettel.campaign.service.CampaignWarningDefaultService;
import com.viettel.campaign.utils.RedisUtil;
import com.viettel.campaign.web.dto.CampaignCustomerDTO;
import com.viettel.campaign.web.dto.CampaignDTO;
import com.viettel.campaign.web.dto.CampaignWarningConfigDTO;
import com.viettel.campaign.web.dto.ResultDTO;
import com.viettel.econtact.filter.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/ipcc/capwcf")
@CrossOrigin(origins = "*")
public class CampaignWarningConfigController {

    private static final Logger logger = LoggerFactory.getLogger(CampaignController.class);

    @Autowired
    CampaignWarningConfigService campaignWarningConfigService;

    @GetMapping(path = "/findAllCampaignWarningByCampaignId")
    @ResponseBody
    public ResponseEntity findAllCampaignWarningByCampaignId(@RequestParam("campaignId") Long campaignId) {
        Iterable<CampaignWarningConfig> listCampaignWarningConfig = campaignWarningConfigService.getAllConfigByCampaignId(campaignId);
        if (listCampaignWarningConfig == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity(listCampaignWarningConfig.iterator(), HttpStatus.OK);
    }

    @RequestMapping(value = "/addNewCampaignWarningConfig", method = RequestMethod.POST)
    public ResultDTO addNewCampaignWarningConfig(@RequestBody List<CampaignWarningConfigDTO> dto) throws ParseException {
        System.out.println("addNewCampaignWarningConfig");
        return campaignWarningConfigService.addNewCampaignWarningConfig(dto);
    }

    @RequestMapping(value = "/deleteCampaignConfigById/{campaignId}", method = RequestMethod.POST)
    public ResultDTO deleteCampaignConfigById(@PathVariable Long campaignId) throws ParseException {
        ResultDTO result = campaignWarningConfigService.deleteCampaignConfigById(campaignId);
        return result;
    }
}

