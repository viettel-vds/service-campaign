//Company: T-Sulution
//Create: longchin196
//update code: 30/12/2020 11:18
package com.viettel.campaign.web.rest;


import com.viettel.campaign.utils.Config;
import com.viettel.campaign.web.dto.ResultDTO;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.viettel.campaign.service.CampaignMessagerService;
import com.viettel.campaign.web.dto.CampaignMessagerDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/campainmessager")
@CrossOrigin("*")
public class CampaingMessagerController {
    @Autowired
    CampaignMessagerService campaignMessagerService;
    @RequestMapping(method = RequestMethod.POST)
    public ResultDTO addNewCampaignMessager(@RequestBody CampaignMessagerDTO CampaignMessagerDto,HttpServletRequest request) {
        return campaignMessagerService.addNewCampaignMessager(CampaignMessagerDto,request);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResultDTO UpsetCampaignMessager(@RequestBody CampaignMessagerDTO CampaignMessagerDto,HttpServletRequest request) {
        return campaignMessagerService.UpsetCampaignMessager(CampaignMessagerDto,request);
    }

    @RequestMapping(value = "/get",method = RequestMethod.POST)
    public ResultDTO getCampaign(@RequestBody CampaignMessagerDTO campaignMessagerDTO) {
        return campaignMessagerService.getCampaignMessenger(campaignMessagerDTO);
    }

    @RequestMapping(value = "/getExt",method = RequestMethod.POST)
    public ResultDTO getCampaign(@RequestParam Integer currentPage, @RequestParam Integer perPage,@RequestBody CampaignMessagerDTO requestDto)
    {
        return campaignMessagerService.getCampaignMessengerExt(currentPage, perPage,requestDto);
    }
    @RequestMapping(value = "/getLog",method = RequestMethod.GET)
    public ResultDTO getCampaignlog(@RequestParam Integer currentPage, @RequestParam Integer perPage,@RequestParam Long messagerId,@RequestParam  Integer timezoneOffset)
    {
        return campaignMessagerService.getCampaignMessengerLog(currentPage, perPage,messagerId, timezoneOffset);
    }

    @RequestMapping(method= RequestMethod.DELETE)
    public ResultDTO deleteCampaignmessager(@RequestParam long messagerId){
        return campaignMessagerService.delCampaignMessager(messagerId);
    }
    @RequestMapping(value = "/getid", method= RequestMethod.GET)
    public ResultDTO getidCampaignmessager(@RequestParam long messagerId){
        return campaignMessagerService.GetIDCampaignMessager(messagerId);
    }

    @PostMapping(value = "/importFile")
    public ResponseEntity<?> importFile(@RequestPart("file") MultipartFile file,
                                @RequestPart("campaignId") String campaignId,
                                @RequestPart("campaignCode") String campaignCode,
                                @RequestPart("campaignName") String campaignName,
                                @RequestPart("language") String language,
                                HttpServletRequest request)
    {
        try {
            if (file.isEmpty()) {
                return null;
            }
            String path = saveUploadFile(file);
            Map<String, Object> map = campaignMessagerService.readAndValidateCustomer(path, Long.parseLong(campaignId)  , campaignCode, campaignName, language,request);
            String message = (String) map.get("message");
            byte[] content = (byte[]) map.get("content");
            return ResponseEntity.ok()
                    .header("Message", message)
                    .body(content);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    private String saveUploadFile(MultipartFile file) {
        try {
            String currentTime = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss").format(new Date());
            String fileName = FilenameUtils.getBaseName(file.getOriginalFilename()) + "_" + currentTime + "." + FilenameUtils.getExtension(file.getOriginalFilename());
            byte[] content = file.getBytes();
            File uploadFolder = new File(Config.EXCEL_DIR);
            if (!uploadFolder.exists()) {
                uploadFolder.mkdir();
            }
            Path path = Paths.get(Config.EXCEL_DIR, fileName);
            Files.write(path, content);
            return path.toString();
        } catch (Exception e) {

        }
        return null;
    }

    @RequestMapping(value = "/downloadFileTemplate", method = RequestMethod.GET)
    public ResponseEntity<byte[]> downloadFileTemplate(@RequestParam("language") String language,
                                                       HttpServletResponse response) {
        XSSFWorkbook workbook = null;
        byte[] contentReturn = null;
        try {
            String fileName = "import_sms_template.xlsx";
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outputStream;
            workbook = campaignMessagerService.buildTemplate(language);
            outputStream = response.getOutputStream();
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            workbook.write(byteArrayOutputStream);
            contentReturn = byteArrayOutputStream.toByteArray();

        } catch (Exception ex) {
            return new ResponseEntity<byte[]>(null, null, HttpStatus.BAD_REQUEST);
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                }
            }
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
        return new ResponseEntity<byte[]>(contentReturn, headers, HttpStatus.OK);
    }
}
