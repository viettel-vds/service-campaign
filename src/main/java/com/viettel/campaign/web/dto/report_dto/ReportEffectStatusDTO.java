package com.viettel.campaign.web.dto.report_dto;

/* Created by ManhPD on 12/22/2020 */
public class ReportEffectStatusDTO {
    private Long campaignId;
    private Long status;
    private String completeName;
    private Long contactStatusCount;

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public Long getContactStatusCount() {
        return contactStatusCount;
    }

    public void setContactStatusCount(Long contactStatusCount) {
        this.contactStatusCount = contactStatusCount;
    }

    public ReportEffectStatusDTO(Long campaignId, Long status, String completeName, Long contactStatusCount) {
        this.campaignId = campaignId;
        this.status = status;
        this.completeName = completeName;
        this.contactStatusCount = contactStatusCount;
    }

    public ReportEffectStatusDTO() {
    }
}
