package com.viettel.campaign.web.dto;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class CampaignCustomerExtDTO {
    private Long campaignId;
    private String campaignCode;
    private String campaignName;
    private Long customerId;
    private Integer createTime;
    private String callStatus;
    private String contactStatus;
    private Date startDate;
    private Date endDate;
    private Integer campaigncount;
}
