package com.viettel.campaign.web.dto;

import com.viettel.campaign.model.ccms_full.CampaignWarningConfig;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class CampaignWarningConfigDTO extends BaseDTO {

    private Long campaignId;
    private Long status;
    private String color;
    private Long levelStart;
    private Long levelEnd;
    private String createBy;
    private Date createTime;
    private String modifyBy;
    private Date modifyTime;

    public CampaignWarningConfig toEntity() {
        CampaignWarningConfig camp = new CampaignWarningConfig();
        camp.setCampaignId(this.campaignId);
        camp.setColor(this.color);
        camp.setLevelEnd(this.levelEnd);
        camp.setLevelStart(this.levelStart);
        camp.setStatus(this.status);
        camp.setCreateBy(this.createBy);
        camp.setCreateTime(this.createTime);
        camp.setModifyBy(this.modifyBy);
        camp.setModifyTime(this.modifyTime);
        return camp;
    }
}
