package com.viettel.campaign.web.dto.request_dto;

import com.viettel.campaign.web.dto.BaseDTO;
import com.viettel.campaign.web.dto.TimeRangeDialModeDTO;
import com.viettel.campaign.web.dto.TimeZoneDialModeDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class CampaignBlackListCatDTO extends BaseDTO {

    private Long campaignBlacklistCatId;
    private String blacklistName;


}
