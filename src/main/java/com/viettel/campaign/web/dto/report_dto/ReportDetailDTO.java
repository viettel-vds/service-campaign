package com.viettel.campaign.web.dto.report_dto;

/* Created by ManhPD on 12/22/2020 */
public class ReportDetailDTO {
    private Long contactCustResultId;
    private Long campaignId;
    private String campaignName;
    private String campaignCode;
    String startTime;
    String endTime;
    String phoneNumber;
    String name;
    Long customerId;
    String startCall;
    String duarationCall;
    String userName;
    String completeName;
    String description;

    public Long getContactCustResultId() {
        return contactCustResultId;
    }

    public void setContactCustResultId(Long contactCustResultId) {
        this.contactCustResultId = contactCustResultId;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getStartCall() {
        return startCall;
    }

    public void setStartCall(String startCall) {
        this.startCall = startCall;
    }

    public String getDuarationCall() {
        return duarationCall;
    }

    public void setDuarationCall(String duarationCall) {
        this.duarationCall = duarationCall;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ReportDetailDTO(Long contactCustResultId, Long campaignId, String campaignName, String campaignCode, String startTime, String endTime, String phoneNumber, String name, Long customerId, String startCall, String duarationCall, String userName, String completeName, String description) {
        this.contactCustResultId = contactCustResultId;
        this.campaignId = campaignId;
        this.campaignName = campaignName;
        this.campaignCode = campaignCode;
        this.startTime = startTime;
        this.endTime = endTime;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.customerId = customerId;
        this.startCall = startCall;
        this.duarationCall = duarationCall;
        this.userName = userName;
        this.completeName = completeName;
        this.description = description;
    }

    public ReportDetailDTO() {
    }
}
