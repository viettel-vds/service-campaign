package com.viettel.campaign.web.dto.request_dto;

import com.viettel.campaign.web.dto.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

//@Getter
//@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomersDTO extends BaseDTO {
    Long customerId ;
    String name;
    Long status;
    String email;
    String description;
    Long customerType;
    Long blacklistCampaign;
    String blacklistName;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCustomerType() {
        return customerType;
    }

    public void setCustomerType(Long customerType) {
        this.customerType = customerType;
    }

    public Long getBlacklistCampaign() {
        return blacklistCampaign;
    }

    public void setBlacklistCampaign(Long blacklistCampaign) {
        this.blacklistCampaign = blacklistCampaign;
    }

    public String getBlacklistName() {
        return blacklistName;
    }

    public void setBlacklistName(String blacklistName) {
        this.blacklistName = blacklistName;
    }
}
