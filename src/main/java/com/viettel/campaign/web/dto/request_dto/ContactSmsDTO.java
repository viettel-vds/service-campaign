package com.viettel.campaign.web.dto.request_dto;

import com.viettel.campaign.web.dto.BaseDTO;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactSmsDTO extends BaseDTO {
    private int companySiteId;
    private int contactSmsId;
    private String campaignId;
    private String smsCode;
    private String smsName;
    private String content;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;
    private int status;
}
