package com.viettel.campaign.web.dto;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

@Getter
@Setter

public class CalendarHolidayDTO {
    private int calendarHolidayId;
    private Integer calendarId;
    private Long day;
    private Long month;
    private String name;
    private Boolean status;
    private Integer toDay;
    private Integer toMonth;
    private Integer holidayTpye;
}
