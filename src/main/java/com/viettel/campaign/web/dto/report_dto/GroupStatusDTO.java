package com.viettel.campaign.web.dto.report_dto;

import com.viettel.campaign.web.dto.ReportQuestionAnswerDTO;

import java.util.List;

/* Created by ManhPD on 12/22/2020 */
public class GroupStatusDTO {
    public List<ReportEffectStatusDTO> listStatus;
}
