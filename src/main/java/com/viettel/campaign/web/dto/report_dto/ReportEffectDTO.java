package com.viettel.campaign.web.dto.report_dto;

/* Created by ManhPD on 12/22/2020 */
public class ReportEffectDTO {
    private Long campaignId;
    private String campaignCode;
    private String campaignName;
    String startTime;
    String endTime;
    Long customerCount;
    Long statisfied;
    Long normal;
    Long unStatisfied;
    Long follow;
    Long unFollow;
    public Float sumStatisfied(){
        Float total =0f;
        total += statisfied + normal + unStatisfied;
        if(total==0) total = 0.001f; // để tránh th /0
        return total;
    }
    public Float sumFollow(){
        Float total =0f;
        total += follow + unFollow;
        if(total==0) total = 0.001f; // để tránh th /0
        return total;
    }
    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Long getCustomerCount() {
        return customerCount;
    }

    public void setCustomerCount(Long customerCount) {
        this.customerCount = customerCount;
    }

    public Long getStatisfied() {
        return statisfied;
    }

    public void setStatisfied(Long statisfied) {
        this.statisfied = statisfied;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Long getUnStatisfied() {
        return unStatisfied;
    }

    public void setUnStatisfied(Long unStatisfied) {
        this.unStatisfied = unStatisfied;
    }

    public Long getFollow() {
        return follow;
    }

    public void setFollow(Long follow) {
        this.follow = follow;
    }

    public Long getUnFollow() {
        return unFollow;
    }

    public void setUnFollow(Long unFollow) {
        this.unFollow = unFollow;
    }

    public ReportEffectDTO(Long campaignId, String campaignCode, String campaignName, String startTime, String endTime, Long customerCount, Long statisfied, Long normal, Long unStatisfied, Long follow, Long unFollow) {
        this.campaignId = campaignId;
        this.campaignCode = campaignCode;
        this.campaignName = campaignName;
        this.startTime = startTime;
        this.endTime = endTime;
        this.customerCount = customerCount;
        this.statisfied = statisfied;
        this.normal = normal;
        this.unStatisfied = unStatisfied;
        this.follow = follow;
        this.unFollow = unFollow;
    }

    public ReportEffectDTO() {
    }
}
