package com.viettel.campaign.web.dto;

/* Created by ManhPD on 12/21/2020 */
public class ReportQuestionAnswerDTO {
    private Long campaignId;
    private String campaignCode;
    private Long scenarioQuestionId;
    private String question;
    private Long scenarioAnswerId;
    private String answer;
    private Long answerCount;
    private Long contactCusResultId;
    private Long questionOrderIndex;
    private Long answerOrderIndex;
    public ReportQuestionAnswerDTO(Long campaignId, String campaignCode, Long scenarioQuestionId, String question, Long scenarioAnswerId, String answer, Long answerCount) {
        this.campaignId = campaignId;
        this.campaignCode = campaignCode;
        this.scenarioQuestionId = scenarioQuestionId;
        this.question = question;
        this.scenarioAnswerId = scenarioAnswerId;
        this.answer = answer;
        this.answerCount = answerCount;
    }

    public ReportQuestionAnswerDTO(Long campaignId, String campaignCode, Long scenarioQuestionId, String question, Long scenarioAnswerId, String answer, Long answerCount, Long questionOrderIndex, Long answerOrderIndex) {
        this.campaignId = campaignId;
        this.campaignCode = campaignCode;
        this.scenarioQuestionId = scenarioQuestionId;
        this.question = question;
        this.scenarioAnswerId = scenarioAnswerId;
        this.answer = answer;
        this.answerCount = answerCount;
        this.contactCusResultId = contactCusResultId;
        this.questionOrderIndex = questionOrderIndex;
        this.answerOrderIndex = answerOrderIndex;
    }

    public Long getQuestionOrderIndex() {
        return questionOrderIndex;
    }

    public void setQuestionOrderIndex(Long questionOrderIndex) {
        this.questionOrderIndex = questionOrderIndex;
    }

    public Long getAnswerOrderIndex() {
        return answerOrderIndex;
    }

    public void setAnswerOrderIndex(Long answerOrderIndex) {
        this.answerOrderIndex = answerOrderIndex;
    }

    public Long getContactCusResultId() {
        return contactCusResultId;
    }

    public void setContactCusResultId(Long contactCusResultId) {
        this.contactCusResultId = contactCusResultId;
    }

    public ReportQuestionAnswerDTO() {
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public Long getScenarioQuestionId() {
        return scenarioQuestionId;
    }

    public void setScenarioQuestionId(Long scenarioQuestionId) {
        this.scenarioQuestionId = scenarioQuestionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Long getScenarioAnswerId() {
        return scenarioAnswerId;
    }

    public void setScenarioAnswerId(Long scenarioAnswerId) {
        this.scenarioAnswerId = scenarioAnswerId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Long getAnswerCount() {
        return answerCount;
    }

    public void setAnswerCount(Long answerCount) {
        this.answerCount = answerCount;
    }
}
