package com.viettel.campaign.web.dto.kafka_dto;

import com.viettel.campaign.web.dto.CampaignDTO;

/* Created by ManhPD on 1/13/2021 */
public class KafkaRequestDTO {
    KafkaCustomerListDTO customerList;
    CampaignDTO campaign;

    public CampaignDTO getCampaign() {
        return campaign;
    }

    public void setCampaign(CampaignDTO campaign) {
        this.campaign = campaign;
    }

    public KafkaCustomerListDTO getCustomerList() {
        return customerList;
    }

    public void setCustomerList(KafkaCustomerListDTO customerList) {
        this.customerList = customerList;
    }
}
