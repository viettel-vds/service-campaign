package com.viettel.campaign.web.dto.kafka_dto;

import com.viettel.campaign.web.dto.CustomerDTO;

import java.util.Date;
import java.util.List;

/* Created by ManhPD on 1/14/2021 */
public class KafkaCustomerDTO {
    String code;
    String description;
    Long siteId;
    Short gender;
    String currentAddress;
    String placeOfBirth;
    Date dateOfBirth;
    String mobileNumber;
    String email;
    Long customerType;
    List<KafkaListcustomizeFieldDTO> listcustomizeField;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Short getGender() {
        return gender;
    }

    public void setGender(Short gender) {
        this.gender = gender;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getCustomerType() {
        return customerType;
    }

    public void setCustomerType(Long customerType) {
        this.customerType = customerType;
    }

    public List<KafkaListcustomizeFieldDTO> getListcustomizeField() {
        return listcustomizeField;
    }

    public void setListcustomizeField(List<KafkaListcustomizeFieldDTO> listcustomizeField) {
        this.listcustomizeField = listcustomizeField;
    }
}
