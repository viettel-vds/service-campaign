package com.viettel.campaign.web.dto.request_dto;

import com.viettel.campaign.web.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BlacklistRequestDTO extends BaseDTO {
    Long blacklistId;
    String blacklistName;
}
