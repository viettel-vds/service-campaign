package com.viettel.campaign.web.dto.request_dto;

import com.viettel.campaign.web.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CampaignTypeBlackListRequestDTO extends BaseDTO {
    Long campaignTypeId;
    String campaignTypeName;
    Long campaignBlacklistCatId;
    String blackListCampaignName;
    private Long campaignBlacklistConfigId;
}
