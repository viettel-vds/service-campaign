package com.viettel.campaign.web.dto.report_dto;

/* Created by ManhPD on 12/22/2020 */
public class ReportCusInfoDTO {
    private Long customerId;
    private String title;
    private String value;
    private Long contactCustResultId;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getContactCustResultId() {
        return contactCustResultId;
    }

    public void setContactCustResultId(Long contactCustResultId) {
        this.contactCustResultId = contactCustResultId;
    }

    public ReportCusInfoDTO(Long customerId, String title, String value, Long contactCustResultId) {
        this.customerId = customerId;
        this.title = title;
        this.value = value;
        this.contactCustResultId = contactCustResultId;
    }

    public ReportCusInfoDTO() {
    }
}
