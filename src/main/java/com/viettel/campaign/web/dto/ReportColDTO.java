package com.viettel.campaign.web.dto;

/* Created by ManhPD on 12/21/2020 */
public final class ReportColDTO {
    public interface PROC_GET_QUESTION_ANSWER2_BY_CAMPAIGN {
        String CAMPAIGN_ID = "CAMPAIGN_ID";
        String CAMPAIGN_CODE = "CAMPAIGN_CODE";
        String SCENARIO_QUESTION_ID = "SCENARIO_QUESTION_ID";
        String SCENARIO_ANSWER_ID = "SCENARIO_QUESTION_ID";
        String QUESTION = "QUESTION";
        String ANSWER = "ANSWER";
        String ANSWER_COUNT = "ANSWER_COUNT";
        String QUESTION_ORDER_INDEX = "QUESTION_ORDER_INDEX";
        String ANSWER_ORDER_INDEX = "ANSWER_ORDER_INDEX";
    }
    public interface QUESTION_ANSWER{
        String CAMPAIGN_ID = "CAMPAIGN_ID";
        String CAMPAIGN_CODE = "CAMPAIGN_CODE";
        String SCENARIO_QUESTION_ID = "SCENARIO_QUESTION_ID";
        String SCENARIO_ANSWER_ID = "SCENARIO_QUESTION_ID";
        String QUESTION = "QUESTION";
        String ANSWER = "ANSWER";
        String ANSWER_COUNT = "ANSWER_COUNT";
        String CONTACT_CUST_RESULT_ID = "CONTACT_CUST_RESULT_ID";
        String QUESTION_ORDER_INDEX = "QUESTION_ORDER_INDEX";
        String ANSWER_ORDER_INDEX = "ANSWER_ORDER_INDEX";
    }
    public interface PROC_GET_STATISTIC_BY_CAMPAIGNCODE {
        String CAMPAIGN_ID = "CAMPAIGN_ID";
        String CAMPAIGN_CODE = "CAMPAIGN_CODE";
        String CONTACT_STATUS = "CONTACT_STATUS";
        String SCENARIO_ANSWER_ID = "SCENARIO_QUESTION_ID";
        String COMPLETE_NAME = "COMPLETE_NAME";
        String AMOUNT = "AMOUNT";
        String QUESTION = "Thống kê kết nối";
    }
    public interface PROC_GET_REPORT_EFECT_BY_CAMPAIGN {
        String CAMPAIGN_ID = "CAMPAIGN_ID";
        String CAMPAIGN_NAME = "CAMPAIGN_NAME";
        String START_TIME = "START_TIME";
        String END_TIME = "END_TIME";
        String CUSTOMER_COUNT = "CUSTOMER_COUNT";
        String CAMPAIGN_CODE = "CAMPAIGN_CODE";
        String SATISFIED = "SATISFIED";
        String NORMAL = "NORMAL";
        String COMPLETE_NAME = "COMPLETE_NAME";
        String UNSATISFIED = "UNSATISFIED";
        String FOLLOW = "FOLLOW";
        String UNFOLLOW = "UNFOLLOW";
        String CONTACT_STATUS = "CONTACT_STATUS";
        String CONTACT_STATUS_COUNT = "CONTACT_STATUS_COUNT";
        String CALL_STATUS = "CALL_STATUS";
        String CALL_STATUS_COUNT = "CALL_STATUS_COUNT";
    }
    public interface PROC_GET_REPORT_DETAIL_BY_CAMPAIGN {
        String CONTACT_CUST_RESULT_ID = "CONTACT_CUST_RESULT_ID";
        String CAMPAIGN_ID = "CAMPAIGN_ID";
        String CAMPAIGN_NAME = "CAMPAIGN_NAME";
        String START_TIME = "START_TIME";
        String END_TIME = "END_TIME";
        String PHONE_NUMBER = "PHONE_NUMBER";
        String CAMPAIGN_CODE = "CAMPAIGN_CODE";
        String CUSTOMER_ID = "CUSTOMER_ID";
        String START_CALL = "START_CALL";
        String DURATION_CALL = "DURATION_CALL";
        String USER_NAME = "USER_NAME";
        String COMPLETE_NAME = "COMPLETE_NAME";
        String DESCRIPTION = "DESCRIPTION";
        String NAME = "NAME";
        String TITLE = "TITLE";
        String VALUE = "VALUE";
    }
    public static Long PAGE = 0l;
    public static Long PAGE_SIZE = 1000l;
}
