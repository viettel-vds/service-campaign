package com.viettel.campaign.web.dto;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ApiResultPagingDTO {
    public Integer total ;
    public Integer perPage ;
    public Integer currentPage ;
    public Integer lastPage ;
    public Object apiResult ;
}
