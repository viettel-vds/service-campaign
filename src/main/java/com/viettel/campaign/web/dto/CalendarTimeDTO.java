package com.viettel.campaign.web.dto;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class CalendarTimeDTO {
    private int calendarTimeId;
    private Integer calendarId;
    private Long day;
    private Long fromHour;
    private Long fromMinute;
    private Long toHour;
    private Long toMinute;
    private Boolean status;
    private Long type;
    private Long calendarType;
    private Long fromSecond;
    private Long toSecond;
    private Integer calendarCount;
}
