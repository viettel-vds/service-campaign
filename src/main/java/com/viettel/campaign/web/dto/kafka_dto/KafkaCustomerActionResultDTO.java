package com.viettel.campaign.web.dto.kafka_dto;

import java.util.Date;

public class KafkaCustomerActionResultDTO {
    Long contactCustResultId;
    Date createTime;
    Long customerId;
    String phoneNumber;
    Long action;

    public KafkaCustomerActionResultDTO(Long contactCustResultId, Date createTime, Long customerId, String phoneNumber, Long action) {
        this.contactCustResultId = contactCustResultId;
        this.createTime = createTime;
        this.customerId = customerId;
        this.phoneNumber = phoneNumber;
        this.action = action;
    }

    public Long getContactCustResultId() {
        return contactCustResultId;
    }

    public void setContactCustResultId(Long contactCustResultId) {
        this.contactCustResultId = contactCustResultId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getAction() {
        return action;
    }

    public void setAction(Long action) {
        this.action = action;
    }
}
