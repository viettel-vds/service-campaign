package com.viettel.campaign.web.dto.report_dto;

import com.viettel.campaign.web.dto.ReportQuestionAnswerDTO;

import java.util.ArrayList;
import java.util.List;

/* Created by ManhPD on 12/21/2020 */
public class GroupQuestion {
    public List<ReportQuestionAnswerDTO> listAnswer;
    public Long totalCount;

    public GroupQuestion(List<ReportQuestionAnswerDTO> listAnswer, Long totalCount) {
        this.listAnswer = listAnswer;
        this.totalCount = totalCount;
    }

    public GroupQuestion() {
        listAnswer = new ArrayList<>();
        totalCount = 0l;
    }
}
