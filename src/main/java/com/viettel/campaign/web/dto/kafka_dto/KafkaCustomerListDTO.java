package com.viettel.campaign.web.dto.kafka_dto;

import java.util.List;

/* Created by ManhPD on 1/14/2021 */
public class KafkaCustomerListDTO {
    String campaignCode;
    List<KafkaCustomerDTO> customers;

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public List<KafkaCustomerDTO> getCustomers() {
        return customers;
    }

    public void setCustomers(List<KafkaCustomerDTO> customers) {
        this.customers = customers;
    }
}
