package com.viettel.campaign.web.dto.kafka_dto;

/* Created by ManhPD on 1/14/2021 */
public class KafkaListcustomizeFieldDTO {
    String key;
    String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
