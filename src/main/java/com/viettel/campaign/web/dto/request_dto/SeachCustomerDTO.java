package com.viettel.campaign.web.dto.request_dto;

import java.io.Serializable;

public class SeachCustomerDTO implements Serializable {
    private Long siteId;
    private String name;
    private String phone;
    private String email;
    private String contact;
    private Long blackListCampaign;
    private String kazooAccountId;

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getKazooAccountId() {
        return kazooAccountId;
    }

    public void setKazooAccountId(String kazooAccountId) {
        this.kazooAccountId = kazooAccountId;
    }

    public Long getBlackListCampaign() {
        return blackListCampaign;
    }

    public void setBlackListCampaign(Long blackListCampaign) {
        this.blackListCampaign = blackListCampaign;
    }
}
