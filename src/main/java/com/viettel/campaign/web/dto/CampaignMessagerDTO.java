package com.viettel.campaign.web.dto;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class CampaignMessagerDTO{
    private Long messagerId;
    private Long messagerIdLog;
    private String messagerTitle;
    private String messagerTitleExt;
    private String mesagerContent;
    private String messagerCustomer;
    private Date messagerDate;
    private String campaignCode;
    private Date startDate;
    private Date endDate;
    private Boolean isDelete;
    private String messagerCode;
    private String campaignName;
    private Integer campaigncount;
    private Long campaignId;
    private String messagerTitleNew;
    private String mesagerContentNew;
    private Integer timezoneOffset;
}
