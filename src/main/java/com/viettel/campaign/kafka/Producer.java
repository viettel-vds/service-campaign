package com.viettel.campaign.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.viettel.campaign.service.CampaignExecuteService;
import com.viettel.campaign.web.dto.CampaignDTO;
import com.viettel.campaign.web.dto.ContactCustResultDTO;
import com.viettel.campaign.web.dto.kafka_dto.KafkaCustomerActionResultDTO;
import com.viettel.campaign.web.dto.request_dto.CampaignRequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.ArrayList;
import java.util.List;

public class Producer {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    CampaignExecuteService campaignExecuteService;
    public Boolean returnResultToVDS(CampaignDTO campaignDTO){
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        List<KafkaCustomerActionResultDTO> ListCustomerActionResultDTO = new ArrayList<>();
        CampaignRequestDTO dto = new CampaignRequestDTO();
        dto.setCampaignCode(campaignDTO.getCampaignCode());
        dto.setFromDate(campaignDTO.getStartTime().toString());
        dto.setToDate(campaignDTO.getEndTime().toString());
        dto.setPage(0);
        //dto.setPageSize();
        List<ContactCustResultDTO> list = (List<ContactCustResultDTO>) campaignExecuteService.searchInteractiveResult(dto).getListData();
        String json = "";
        try {
            json = ow.writeValueAsString(ListCustomerActionResultDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        kafkaTemplate.send("customer_action_result", json);

        return true;
    }
}
