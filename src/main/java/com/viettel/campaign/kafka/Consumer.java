package com.viettel.campaign.kafka;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.viettel.campaign.model.ccms_full.Campaign;
import com.viettel.campaign.model.ccms_full.Customer;
import com.viettel.campaign.service.CampaignService;
import com.viettel.campaign.service.impl.CustomerServiceImpl;
import com.viettel.campaign.web.dto.CampaignCustomerDTO;
import com.viettel.campaign.web.dto.CampaignDTO;
import com.viettel.campaign.web.dto.ResultDTO;
import com.viettel.campaign.web.dto.kafka_dto.KafkaCustomerDTO;
import com.viettel.campaign.web.dto.kafka_dto.KafkaRequestDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/* Created by ManhPD on 1/13/2021 */
@Component
public class Consumer {
    @Autowired
    CustomerServiceImpl customerService;
    @Autowired
    CampaignService campaignService;

    private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);
    @KafkaListener(topics = "test1", groupId = "group_manh_id")
    public void listenGroupFoo(String message) {
        ObjectMapper jacksonObjMapper = new ObjectMapper();
        JsonNode jsonNode;
        try {
            KafkaRequestDTO requestDto = new ObjectMapper().readValue(message, KafkaRequestDTO.class);
            List<KafkaCustomerDTO> customerDTOList = requestDto.getCustomerList().getCustomers();
            CampaignDTO campaignDTO = requestDto.getCampaign();
            String campaignCode = campaignDTO.getCampaignCode();
            // update or insert campaign
            Campaign campaign = campaignService.findCampaignActiveByCode(campaignCode);
            if(campaign != null && campaign.getCampaignId() != null){
                // update
                campaignDTO.setCampaignId(campaign.getCampaignId());
                campaignService.updateCampaign(campaignDTO);
                LOGGER.info("--- Kafka:: update CampaignCode: "+campaignDTO.getCampaignCode() +" success");
            } else {
                // insert
                campaign = (Campaign) campaignService.addNewCampaign(campaignDTO).getData();
                LOGGER.info("--- Kafka:: insert CampaignCode: "+campaignDTO.getCampaignCode() +" success");
            }
            // update or insert customer
            List<Customer> customerListSaved = customerService.createOrUpdateListCustomerByKafka(customerDTOList);

            // add customer to campaign
            // get cusid
            StringBuilder lstCustomerId = new StringBuilder();
            for (Customer cus : customerListSaved) {
                lstCustomerId.append(cus.getCustomerId() + ",");
            }
            // add to campaign
            CampaignCustomerDTO campaignCustomerDTO = new CampaignCustomerDTO();
            campaignCustomerDTO.setCompanySiteId(campaignDTO.getCompanySiteId());
            campaignCustomerDTO.setCampaignId(campaign.getCampaignId());
            campaignCustomerDTO.setLstCustomerId(lstCustomerId.toString());
            customerService.addCustomerToCampaign(campaignCustomerDTO);
            LOGGER.info("--- Kafka:: add customer to CampaignCode: "+campaignDTO.getCampaignCode() +" success");
        }catch (ParseException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        System.out.println("Received Message in group_manh_id: " + message);
    }
}
