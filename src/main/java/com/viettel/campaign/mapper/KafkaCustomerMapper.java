package com.viettel.campaign.mapper;

import com.viettel.campaign.model.ccms_full.Customer;
import com.viettel.campaign.web.dto.CustomerDTO;
import com.viettel.campaign.web.dto.kafka_dto.KafkaCustomerDTO;

/* Created by ManhPD on 1/14/2021 */
public class KafkaCustomerMapper {
    public Customer toPersistenceBean(KafkaCustomerDTO dtoBean){
        Customer obj = new Customer();

        if (dtoBean != null) {
            obj = new Customer();
            obj.setCode(dtoBean.getCode());
//            obj.setName(dtoBean.getName());
            obj.setDescription(dtoBean.getDescription());
//            obj.setCompanyName(dtoBean.getCompanyName());
//            obj.setCustomerImg(dtoBean.getCustomerImg());
//            obj.setCreateDate(dtoBean.getCreateDate());
//            obj.setUpdateDate(dtoBean.getUpdateDate());
//            obj.setStatus(Long.parseLong(dtoBean.getStatus()));
//            obj.setCreateBy(dtoBean.getCreateBy());
//            obj.setUpdateBy(dtoBean.getUpdateBy());
            obj.setSiteId(dtoBean.getSiteId());
            obj.setGender(dtoBean.getGender());
            obj.setCurrentAddress(dtoBean.getCurrentAddress());
            obj.setPlaceOfBirth(dtoBean.getPlaceOfBirth());
            obj.setDateOfBirth(dtoBean.getDateOfBirth());
            obj.setMobileNumber(dtoBean.getMobileNumber());
            obj.setEmail(dtoBean.getEmail());
            //obj.setUserName(dtoBean.getUserName());
            //obj.setAreaCode(dtoBean.getAreaCode());
            obj.setCustomerType(dtoBean.getCustomerType());
            //obj.setCallAllowed(Long.parseLong(dtoBean.getCallAllowed()));
            //obj.setEmailAllowed(dtoBean.getEmailAllowed());
            //obj.setSmsAllowed(dtoBean.getSmsAllowed());
            //obj.setIpccStatus(dtoBean.getIpccStatus());
        }

        return obj;
    }

    public Customer updatePersistenceBean(KafkaCustomerDTO dtoBean,Customer obj){
        if(dtoBean == null) return obj;
        if(dtoBean.getCode() != null && !dtoBean.getCode().isEmpty()) obj.setCode(dtoBean.getCode());
        if(dtoBean.getDescription() != null && !dtoBean.getDescription().isEmpty()) obj.setDescription(dtoBean.getDescription());
        if(dtoBean.getSiteId() != null ) obj.setSiteId(dtoBean.getSiteId());
        if(dtoBean.getGender() != null ) obj.setGender(dtoBean.getGender());
        if(dtoBean.getCurrentAddress() != null && !dtoBean.getCurrentAddress().isEmpty()) obj.setCurrentAddress(dtoBean.getCurrentAddress());
        if(dtoBean.getPlaceOfBirth() != null && !dtoBean.getPlaceOfBirth().isEmpty()) obj.setPlaceOfBirth(dtoBean.getPlaceOfBirth());
        if(dtoBean.getDateOfBirth() != null ) obj.setDateOfBirth(dtoBean.getDateOfBirth());
        if(dtoBean.getMobileNumber() != null && !dtoBean.getMobileNumber().isEmpty()) obj.setMobileNumber(dtoBean.getMobileNumber());
        if(dtoBean.getEmail() != null && !dtoBean.getEmail().isEmpty()) obj.setEmail(dtoBean.getEmail());
        if(dtoBean.getCustomerType() != null ) obj.setCustomerType(dtoBean.getCustomerType());

//            obj.setName(dtoBean.getName());
//            obj.setDescription(dtoBean.getDescription());
//            obj.setCompanyName(dtoBean.getCompanyName());
//            obj.setCustomerImg(dtoBean.getCustomerImg());
//            obj.setCreateDate(dtoBean.getCreateDate());
//            obj.setUpdateDate(dtoBean.getUpdateDate());
//            obj.setStatus(Long.parseLong(dtoBean.getStatus()));
//            obj.setCreateBy(dtoBean.getCreateBy());
//            obj.setUpdateBy(dtoBean.getUpdateBy());
//            obj.setSiteId(dtoBean.getSiteId());
//            obj.setGender(dtoBean.getGender());
//            obj.setCurrentAddress(dtoBean.getCurrentAddress());
//            obj.setPlaceOfBirth(dtoBean.getPlaceOfBirth());
//            obj.setDateOfBirth(dtoBean.getDateOfBirth());
//            obj.setMobileNumber(dtoBean.getMobileNumber());
//            obj.setEmail(dtoBean.getEmail());
            //obj.setUserName(dtoBean.getUserName());
            //obj.setAreaCode(dtoBean.getAreaCode());
            //obj.setCustomerType(dtoBean.getCustomerType());
            //obj.setCallAllowed(Long.parseLong(dtoBean.getCallAllowed()));
            //obj.setEmailAllowed(dtoBean.getEmailAllowed());
            //obj.setSmsAllowed(dtoBean.getSmsAllowed());
            //obj.setIpccStatus(dtoBean.getIpccStatus());
        return obj;
    }
}
