package com.viettel.campaign.mapper;

import com.viettel.campaign.model.ccms_full.CustomerContact;
import com.viettel.campaign.web.dto.CustomerContactDTO;

public class CustomerContactMapper extends BaseMapper<CustomerContact, CustomerContactDTO> {

    @Override
    public CustomerContactDTO toDtoBean(CustomerContact model) {
        CustomerContactDTO obj = null;
        if (model != null) {
            obj = new CustomerContactDTO();
            obj.setContactId(model.getContactId());
            obj.setCustomerId(model.getCustomerId());
            obj.setContactType(model.getContactType());
            obj.setContact(model.getContact());
            obj.setIsDirectLine(model.getIsDirectLine());
            obj.setStatus(model.getStatus());
        }
        return obj;
    }

    @Override
    public CustomerContact toPersistenceBean(CustomerContactDTO dtoBean) {
        CustomerContact obj = null;
        if (dtoBean != null) {
            obj = new CustomerContact();
            obj.setContactId(dtoBean.getContactId());
            obj.setCustomerId(dtoBean.getCustomerId());
            obj.setContactType(dtoBean.getContactType());
            obj.setContact(dtoBean.getContact());
            obj.setIsDirectLine(dtoBean.getIsDirectLine());
            obj.setStatus(dtoBean.getStatus());
        }
        return obj;
    }
}
